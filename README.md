Stream-aware multipath schedulers
=================================

The goal of these tools is to help design stream-aware multipath scheduling algorithms, with **MPQUIC** in mind.

## Simulator

**Multipathsim** is a discrete-event simulator to help explore various scheduling strategies for **MPQUIC**.

This is an earlier work designed for exploration, and it is **not** designed to be realistic.  You might still
find it useful, and it has a nice packet-based visualization.

See its [README](simulation/README.md) for details.

## Model

This one is a simple network model (fixed latency and capacity) with lots of different scheduling algorithms.
It allows to really concentrate on the pros and cons of each algorithm.

Instances (i.e. paths and streams) can be written by hand or imported from a trace from <https://www.webpagetest.org>.

See its [README](model/README.md) for details.

License
=======

All code in the repository is available under the terms of the CeCILL-B license (BSD-compatible), see `LICENSE.txt`.
