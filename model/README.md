Continuous model
================

This folder contains code to simulate stream-aware multipath schedulers in a simple continuous model:
each path has a fixed latency and a fixed capacity (maximum throughput).

Input files
===========

The input file describes the set of paths, each with a "speed" (network capacity) and "delay".

It also describes the set of streams, with implicit IDs numbered from 0:

- release date: when does the stream "appears", i.e. becomes available for sending
- size: what is the total amount of data that the stream wants to send
- optional dependency: does this stream depend on another stream to complete before it can start?
  (ID of the other stream)
- optional release delay: after the stream on which we depend has completed, apply the given delay
  before we become available.  This could model computation, web page parsing, database or disk lookup...

All times (speed, delay, release time) are in arbitrary time unit, but must be consistent with the size unit.
When in doubt, use bytes and seconds.  An alternative and more readable convention for small examples can be:

- delay and release time in ms
- size in bytes
- speed in bytes/ms i.e. KB/s

See `instances/` for example.

Policy-based solver
===================

A scheduling policy is run at each interesting time step, and given the current state
of the system, must take a scheduling decision.

Program:

    solve_policy.py

List of scheduling policies: see `scheduling.py` (at the end).

Omniscient solver
=================

An omniscient solver knows about future streams and about dependencies.  Thus, it can anticipate
the future to better optimise overall performance.

Program:

    solve_omniscient.py

List of omniscient algorithms: run the program without arguments.

LP-based solver
===============

Program:

    solve_optimal.py

This solver is currently abandoned, because it is very complex and also very slow.  It uses an external solver through
the Numberjack library.

There are two modes:

- stream breaks
- path breaks

The main idea is to formulate the optimisation problem as a LP problem.  There is a parameter that sets the maximum
number of **breaks** for each stream or path.  Then the solver tries to optimise the position of these breaks and
the allocation of which stream uses which path between two breaks.

License
=======

This program is available under the terms of the CeCILL-B license (BSD-compatible), see `LICENSE.txt`.
