"""
Discrete event simulation for solving a problem in the continuous model.

The idea is to run the specified scheduling algorithm at each interesting
point in time, and ask it what allocation it wants to make and for how long.
"""

from collections import defaultdict
from itertools import groupby
import sys

import salabim as sim

from constants import EPSILON
from utils import Allocation


class ActiveStream(object):
    """Like a Stream, but with a mutable attribute "remaining_size"."""
    __slots__ = ["id", "release_time", "size", "remaining_size"]

    def __init__(self, stream):
        self.id = stream.id
        self.release_time = stream.release_time
        self.size = stream.size
        self.remaining_size = stream.size

    def __str__(self):
        attrs = ["{}={}".format(attr, getattr(self, attr)) for attr in self.__slots__]
        return '{}({})'.format(type(self).__name__, ", ".join(attrs))

    def __repr__(self):
        return str(self)


class ActivePath(object):
    """Like a Path, but with a mutable attribute "finish_dates" that maps
    stream IDs to the date at which the given stream will finish
    propagating on this path.
    """
    __slots__ = ["id", "speed", "delay", "finish_dates"]

    def __init__(self, path):
        self.id = path.id
        self.speed = path.speed
        self.delay = path.delay
        self.finish_dates = defaultdict(int)

    def update_finish_date(self, stream_id, now):
        self.finish_dates[stream_id] = max(self.finish_dates[stream_id],
                                           now + self.delay)

    def in_flight_finish_date(self, stream_id):
        return self.finish_dates[stream_id]

    def cleanup_finish_date(self, stream_id):
        if stream_id in self.finish_dates:
            del self.finish_dates[stream_id]

    def cleanup_obsolete_finish_dates(self, now):
        for stream_id in [s for s, d in self.finish_dates.items() if d < now]:
            del self.finish_dates[stream_id]

    def __str__(self):
        attrs = ["{}={}".format(attr, getattr(self, attr)) for attr in self.__slots__ if attr != "finish_dates"]
        attrs.append("finish_dates={" + ", ".join(["{}: {:.2f}".format(stream_id, finish) for stream_id, finish in self.finish_dates.items()]) + "}")
        return '{}({})'.format(type(self).__name__, ", ".join(attrs))

    def __repr__(self):
        return str(self)


class Action(object):
    __slots__ = ["active_stream", "path", "duration"]

    def __init__(self, active_stream, path, duration):
        self.active_stream = active_stream
        self.path = path
        self.duration = duration

    def __str__(self):
        attrs = ["{}={}".format(attr, getattr(self, attr)) for attr in self.__slots__]
        return '{}({})'.format(type(self).__name__, ", ".join(attrs))

    def __repr__(self):
        return str(self)


class StreamRelease(sim.Component):

    def setup(self, simulation, release_time, streams):
        self.simulation = simulation
        self.release_time = release_time
        self.streams = streams

    def process(self):
        yield self.hold(self.release_time - self.env.now())
        self.simulation.add_streams(self.streams)
        # This will make the simulation leave its hold()ing state.
        self.simulation.activate()


class Simulation(sim.Component):

    def setup(self, scheduling_policy, problem):
        self.problem = problem
        self.policy = scheduling_policy
        self.paths = [ActivePath(p) for p in problem.paths]
        self.active_streams = list()
        # The solution is built while the simulation progresses
        self.solution = list()
        # Create "stream release" events for each release time.
        # We filter out streams that have a dependency, they will be added later.
        dependencyless_streams = [s for s in problem.streams if s.release_after == None]
        release_time_f = lambda stream: stream.release_time
        groups = [(release_time, list(streams))
                  for (release_time, streams)
                  in groupby(sorted(dependencyless_streams, key=release_time_f),
                             release_time_f)]
        for (release_time, new_streams) in groups:
            StreamRelease("StreamRelease." + str(release_time),
                          simulation=self,
                          release_time=release_time,
                          streams=new_streams)

    def cleanup_obsolete_finish_dates(self):
        """Should be called from time to time to remove obsolete in-flight account
        data from paths.  This is mostly useful to save memory and to help
        debugging, because printing a path with lots of useless in-flight
        information would make it harder to understand what's going on.
        """
        for p in self.paths:
            p.cleanup_obsolete_finish_dates(self.env.now())

    def add_streams(self, streams):
        self.cleanup_obsolete_finish_dates()
        self.active_streams.extend([ActiveStream(s) for s in streams])

    def remove_stream(self, stream):
        self.cleanup_obsolete_finish_dates()
        self.active_streams.remove(stream)
        # Compute the finish date at which all in-flight data will be
        # received.
        finish_date = max(p.in_flight_finish_date(stream.id) for p in self.paths)
        # Look for other streams that were depending on this stream completion.
        for s in self.problem.streams:
            if s.release_after == stream.id:
                release_time = max(finish_date, s.release_time)
                if s.release_delay:
                    release_time = max(release_time, finish_date + s.release_delay)
                # Modify instance to record the actual release time of the
                # stream (useful for visualisation)
                s.release_time = release_time
                StreamRelease("StreamRelease." + str(release_time),
                              simulation=self,
                              release_time=release_time,
                              streams=[s])

    def account_for_data_sent(self, active_stream, path, data):
        """Returns True if the stream has finished"""
        # Update finish date on the path
        path.update_finish_date(active_stream.id, self.env.now())
        # Remove sent data
        active_stream.remaining_size -= data
        # Notify scheduler
        self.policy.account_for_data_sent(active_stream, path, data, self.env.now())
        if active_stream.remaining_size <= EPSILON:
            # Notify scheduler
            self.policy.remove_stream(active_stream, self.env.now())
            self.remove_stream(active_stream)
            return True
        else:
            return False

    def process(self):
        # Track actions that did not complete in the previous scheduling
        # runs.
        remaining_actions = []
        while True:
            print("occupation: {},{}".format(self.env.now(), len(self.active_streams)))
            while len(self.active_streams) == 0 and len(remaining_actions) == 0:
                # Wait for next stream release.
                yield self.passivate()
                print("occupation: {},{}".format(self.env.now(), len(self.active_streams)))
            # Stream allocations that will run in parallel
            actions = []
            active_paths = set(self.paths) - {action.path for action in remaining_actions}
            while len(active_paths) > 0:
                action = self.policy.schedule(self.active_streams, list(active_paths), self.paths, self.env.now())
                if action == None:
                    # The scheduling policy decided to stop, even though
                    # some paths are available.  That's OK.
                    break
                if action[2] != None and action[2] < EPSILON:
                    # Ignore action with negative or zero duration
                    print("[{:.2f}] warning: ignoring action with negative or zero duration:".format(self.env.now()),
                          action,
                          file=sys.stderr)
                    continue
                action = Action(active_stream=action[0], path=action[1], duration=action[2])
                #print("{:.2f} {}".format(self.env.now(), action))
                actions.append(action)
                assert(action.path in active_paths)
                active_paths.remove(action.path)
            # Add back remaining actions
            all_actions = actions + remaining_actions
            remaining_actions.clear()
            # Check/fix wrong durations
            streams = list({action.active_stream for action in all_actions})
            for stream in streams:
                # Maximum action duration, i.e. duration after which the stream will complete
                max_duration = None
                current_duration = 0.
                acts = [action for action in all_actions if action.active_stream == stream]
                durations = sorted([action.duration for action in acts if action.duration != None])
                # Add a dummy action with duration 0 to facilitate iteration
                durations.insert(0, current_duration)
                # Compute how much data would be sent on all active paths at the current duration
                total_sent = 0
                for d1, d2 in zip(durations, durations[1:]):
                    current_actions = [action for action in acts if action.duration == None or d2 <= action.duration]
                    current_paths = [action.path for action in current_actions]
                    current_speed = sum(path.speed for path in current_paths)
                    current_durations = [action.duration for action in current_actions]
                    new_total_sent = total_sent + (d2 - d1) * current_speed
                    if new_total_sent > stream.remaining_size:
                        max_duration = d1 + (stream.remaining_size - total_sent) / current_speed
                        d = {'stream': stream.id, 'paths': [path.id for path in current_paths],
                             'durations': ["{:.2f}".format(d) for d in current_durations], 'total_sent': total_sent,
                             'current_duration': current_duration,'remaining': stream.remaining_size - total_sent,
                             'max_duration': max_duration}
                        print("[{:.2f}] warning: actions with too large duration for the remaining size, fixing".format(self.env.now()),
                              file=sys.stderr)
                        print("----> stream={stream}, paths={paths}, durations={durations}, total_sent={total_sent}, current_duration={current_duration:.2f}, remaining={remaining}, max_duration={max_duration:.2f}".format(**d),
                              file=sys.stderr)
                        # Reduce duration for all current actions
                        for action in current_actions:
                            action.duration = max_duration
                        current_duration = max_duration
                        # Stop iteration
                        break
                    else:
                        total_sent = new_total_sent
                        current_duration = d2
                # Now handle actions with no duration: compute a
                # reasonable duration.  It will necessarily be the same
                # for all actions that are related to a given stream.
                actions_noduration = [action for action in acts if action.duration == None]
                if not actions_noduration:
                    continue
                paths_noduration = [action.path for action in actions_noduration]
                speed_noduration = sum(path.speed for path in paths_noduration)
                if max_duration:
                    # Easy: we already exceeded the maximum duration
                    missing_duration = max_duration
                else:
                    # We have to continue the computation from above
                    missing_duration = current_duration + (stream.remaining_size - total_sent) / speed_noduration
                for action in actions_noduration:
                    action.duration = missing_duration
            # Advance simulation
            if len(all_actions) > 0:
                current_date = self.env.now()
                planned_duration = min(action.duration for action in all_actions)
                # This will be (possibly) interrupted by the next stream release.
                yield self.hold(planned_duration)
                # Did we get interrupted by an unexpected event?
                new_date = self.env.now()
                actual_duration = new_date - current_date
                expected_duration = (abs(actual_duration - planned_duration) < EPSILON)
                # Record the actions in the interval that just elapsed, and decrement remaining work.
                #print("New actions for period {:.2f}-{:.2f}:".format(current_date, new_date), actions)
                #print("All actions for period {:.2f}-{:.2f}:".format(current_date, new_date), all_actions)
                for action in all_actions:
                    self.solution.append(Allocation(action.active_stream.id, action.path.id, current_date, new_date))
                    finished = self.account_for_data_sent(action.active_stream, action.path, action.path.speed * actual_duration)
                    action.duration -= actual_duration
                    # Keep the action for the next scheduling run, except
                    # if we were interrupted unexpectedly.
                    if expected_duration and action.duration > EPSILON and not finished:
                        remaining_actions.append(action)
                #print(new_date, self.solution)


def run_simulation(policy, instance):
    """Run a simulation on the given problem instance with the given policy.

    If streams have dependencies, the instance will be modified to record
    the actual release time of these streams.
    """
    env = sim.Environment(trace=False)
    simulation = Simulation("simulation", scheduling_policy=policy, problem=instance)
    env.run()
    return simulation.solution
