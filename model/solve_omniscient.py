#!/usr/bin/env python3

import itertools
import math
import sys

import intervals as I

from constants import EPSILON
import graphical
import utils

"""
Algorithms to compute omniscient solutions, hopefully optimal.
"""

def consistent_priorities(instance, ordered_streams):
    """Check that the stream priority order does not violate stream
    dependencies.  A violation is such that S1 < S2 in the ordering, but
    S1 depends on the completion of S2.
    """
    dependencies = [s for s in ordered_streams if s.release_after != None]
    if len(dependencies) == 0:
        return True
    # Not very efficient -- O(n^2) in the worst case where n is the number
    # of streams -- but this is enough if we have few dependencies.
    for s1 in dependencies:
        # S1 depends on S2
        s2_id = s1.release_after
        for s in ordered_streams:
            # We encounter S2 first: OK
            if s.id == s2_id:
                break
            # We encounter S1 first: violation
            if s.id == s1.id:
                return False
    return True


def allocate_subset(instance, ordered_streams):
    """Given a subset of streams that are already ordered by priority, compute
    a partial solution by allocating the streams to paths.

    Note that this is not simply priority-based ECF, because the streams
    might have different release dates.  A lower-priority stream might
    still be able to send some data while a higher-priority stream has not
    been released yet.

    For simplicity, we compute everything from the point of view of the
    receiver (except for the final solution).
    """
    # Check that the priority order is consistent with stream dependencies
    if not consistent_priorities(instance, ordered_streams):
        return []
    # Time constraints for each stream on each path, i.e. which time
    # intervals can be allocated from the point of view of the receiver.
    # The initial constraints are only due to release time, but they will
    # be updated later due to stream dependencies.
    stream_constraints = {stream.id:
                          {path.id: I.closed(stream.release_time + path.delay, I.inf)
                           for path in instance.paths}
                          for stream in instance.streams}
    #print("stream_constraints =", stream_constraints)
    # What size remains to be allocated for each stream
    remaining_size = {stream.id: stream.size for stream in ordered_streams}
    # Time intervals that have already been allocated for each path (again
    # from the point of view of the receiver)
    allocated = {path.id: I.empty() for path in instance.paths}
    # List of Allocation objects
    solution = []
    for stream in ordered_streams:
        #print("[**] Working on stream", stream.id)
        # If there is a dependency, adjust constraints
        if stream.release_after != None:
            # Compute finish date of the stream we depend on
            finish_date_dep = max(alloc.time_stop + instance.paths[alloc.path_id].delay
                                  for alloc in solution
                                  if alloc.stream_id == stream.release_after)
            # Date at which we can start sending (from the sender point of view)
            start_date = finish_date_dep
            if stream.release_delay != None:
                start_date += stream.release_delay
            # The release time still applies
            start_date = max(start_date, stream.release_time)
            # Update constraints
            for path in instance.paths:
                interval = I.closed(start_date + path.delay, I.inf)
                #print("Changing constraints on path", path, "from", stream_constraints[stream.id][path.id], "to", interval)
                stream_constraints[stream.id][path.id] = interval
            # Update instance (useful for visualisation)
            instance.streams[stream.id].release_time = start_date
        # Allocate slots for this stream
        while remaining_size[stream.id] > EPSILON:
            #print("remaining_size", remaining_size[stream.id])
            # 1. Find a suitable allocation start date: as early as possible
            #  a) start by computing available time intervals for this stream
            available = {path.id: allocated[path.id].complement().intersection(stream_constraints[stream.id][path.id])
                         for path in instance.paths}
            #print("available =", available)
            #  b) then find the earliest starting date among the paths
            start = min([interval.lower for path, interval in available.items()])
            #print("start =", start)
            # 2. Find which paths we can use for this allocation
            paths = list(filter(lambda p: start in available[p.id], instance.paths))
            #print("paths =", paths)
            # 3. Find allocation stop date. This is the earliest date of all reasons to stop:
            #  a) the stream has sent all its data
            stop_stream = start + remaining_size[stream.id] / sum([p.speed for p in paths])
            #print("stop_stream =", stop_stream)
            #  b) one path (that we were using) becomes busy
            stop_path_busy = min(allocated[path.id].intersection(I.open(start, I.inf)).lower for path in paths)
            #print("stop_path_busy =", stop_path_busy)
            #  c) a new path (that we were not already using) becomes available
            unused_paths = [p for p in instance.paths if p not in paths]
            if len(unused_paths) > 0:
                stop_new_path = min(available[path.id].intersection(I.open(start, I.inf)).lower for path in unused_paths)
            else:
                stop_new_path = I.inf
            #print("stop_new_path =", stop_new_path)
            #  final stop date
            stop = min(stop_stream, stop_path_busy, stop_new_path)
            # 4. Record allocation
            for path in paths:
                # Be careful: for the solution, timing is seen from the sender
                allocation = utils.Allocation(stream_id=stream.id,
                                              path_id=path.id,
                                              time_start=start - path.delay,
                                              time_stop=stop - path.delay)
                #print("allocation =", allocation)
                solution.append(allocation)
                allocated[path.id] = allocated[path.id].union(I.closedopen(start, stop))
                remaining_size[stream.id] -= path.speed * (stop - start)
    return solution

def bruteforce_metric(orig_instance, metric):
    """Bruteforce method: try all possible permutations of relative stream
    priorities and keep the best one (i.e. the one that minimizes the metric).

    This assumes that there exists a priority order between streams that
    yields an optimal solution with regards to the metric.

    If some streams have dependencies, the input instance is modified to
    record the actual release time of streams.
    """
    # Very naive for now.  A more efficient algorithm would use some kind
    # of union-find structure to try to group streams into non-interfering
    # subsets.
    min_allocation = None
    min_metric_value = float("NaN")
    min_ordering = None
    min_order_str = None
    for ordering in itertools.permutations(orig_instance.streams):
        if not consistent_priorities(orig_instance, ordering):
            continue
        # Copy instance, because it is modified by the allocation function
        instance = orig_instance.deepcopy()
        order = ", ".join(["S{}".format(stream.id) for stream in ordering])
        print("[*] Trying order", order, file=sys.stderr)
        allocation = allocate_subset(instance, ordering)
        if not allocation:
            print("[*] Error: empty allocation received", file=sys.stderr)
            continue
        # Debug
        #graphical.display_solution(instance, utils.normalize_solution(allocation), order, instance_file)
        metric_value = metric(instance, allocation)
        # Reverse test to work with NaN
        if not metric_value >= min_metric_value:
            min_allocation = allocation
            min_metric_value = metric_value
            min_ordering = ordering
            min_order_str = order
    print("Optimal order:", min_order_str, file=sys.stderr)
    # Compute allocation for the optimal ordering again, to modify the
    # original instance.
    allocate_subset(orig_instance, min_ordering)
    return min_allocation

def bruteforce_avg_completion_time(orig_instance):
    return bruteforce_metric(orig_instance, utils.average_completion_time)

def bruteforce_max_completion_time(orig_instance):
    return bruteforce_metric(orig_instance, utils.max_completion_time)

def bruteforce_avg_stretch(orig_instance):
    return bruteforce_metric(orig_instance, utils.average_stretch)

def bruteforce_max_stretch(orig_instance):
    return bruteforce_metric(orig_instance, utils.max_stretch)

def bruteforce_all_metrics(orig_instance):
    """Bruteforce all possible orderings and compute a set of metrics for each
    order: average response time, max response time, average stretch, max
    stretch.

    We also compute whether a solution is "work-conserving", that is, it
    never leaves a path idle.
    """
    metrics = {"avg_resp": dict(), "max_resp": dict(),
               "avg_stretch": dict(), "max_stretch": dict(),
               "work_conserv": dict()}
    # Manage table for future display of results
    from texttable import Texttable
    table = Texttable()
    header = list(sorted(metrics.keys()))
    table.add_row([""] + header)
    #table.set_cols_dtype(["t", "f", "f", "f", "f", "b"])
    # Compute metrics
    for ordering in itertools.permutations(orig_instance.streams):
        if not consistent_priorities(orig_instance, ordering):
            continue
        # Copy instance, because it is modified by the allocation function
        instance = orig_instance.deepcopy()
        order = ",".join(["S{}".format(stream.id) for stream in ordering])
        allocation = allocate_subset(instance, ordering)
        metrics["avg_resp"][order] = utils.average_completion_time(instance, allocation)
        metrics["max_resp"][order] = utils.max_completion_time(instance, allocation)
        metrics["avg_stretch"][order] = utils.average_stretch(instance, allocation)
        metrics["max_stretch"][order] = utils.max_stretch(instance, allocation)
        metrics["work_conserv"][order] = str(utils.is_work_conserving(instance, allocation))
        table.add_row([order] + [metrics[m][order] for m in header])
    # Display table
    print(table.draw())
    # Debug
    #for ordering in itertools.permutations(instance.streams):
    #    order = ",".join(["S{}".format(stream.id) for stream in ordering])
    #    allocation = allocate_subset(instance, ordering)
    #    graphical.display_solution(instance, utils.normalize_solution(allocation), order, instance_file)


omniscient_algorithms = {
    "bruteforce_avg_completion_time": bruteforce_avg_completion_time,
    "bruteforce_max_completion_time": bruteforce_max_completion_time,
    "bruteforce_avg_stretch": bruteforce_avg_stretch,
    "bruteforce_max_stretch": bruteforce_max_stretch,
    "bruteforce_all_metrics": bruteforce_all_metrics,
}

def usage():
    print("usage: {} <algorithm> <instance>".format(sys.argv[0]), file=sys.stderr)
    print("List of possible algorithms:", file=sys.stderr)
    for algo in omniscient_algorithms:
        print("[*]", algo, file=sys.stderr)

if __name__ == '__main__':
    if len(sys.argv) <= 2:
        usage()
        exit(-1)
    algorithm_name = sys.argv[1]
    instance_file = sys.argv[2]
    if algorithm_name not in omniscient_algorithms:
        print("Error: algorithm not found", file=sys.stderr)
        usage()
        exit(-1)
    algorithm = omniscient_algorithms[algorithm_name]
    instance = utils.ProblemInstance(instance_file)
    solution = algorithm(instance)
    if solution:
        solution = utils.normalize_solution(solution)
        utils.print_solution(solution)
        utils.print_completion_times(instance, solution)
        graphical.display_solution(instance, solution, algorithm_name, instance_file)
