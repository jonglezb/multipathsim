#!/usr/bin/env Rscript

library(ggplot2)
library(dplyr)

raw_data <- read.csv(commandArgs(TRUE)[1])
title <- commandArgs(TRUE)[2]
output <- commandArgs(TRUE)[3]

data <- raw_data %>% group_by(Scheduler) %>% mutate(Duration=lead(Date)-Date) %>% filter(Occupation!=0)

#data <- data %>% filter(Scheduler=="SRPT-ECF") %>% group_by(Occupation) %>% summarise(OccupationDuration=sum(Duration))

summary(data)

integer_breaks <- function(n = 5, ...) {
  fxn <- function(x) {
    breaks <- floor(pretty(x, n, ...))
    names(breaks) <- attr(breaks, "labels")
    breaks
  }
  return(fxn)
}

graph <- ggplot(data, aes(x=Occupation, weight=Duration, linetype=Scheduler, color=Scheduler)) +
#         geom_step(aes(y = 1 - ..y..), stat='ecdf') +
         geom_freqpoly(stat='count') +
#         scale_y_log10(breaks=c(1, 1e-1, 1e-2, 1e-3, 1e-4, 1e-5), labels=c('100%', '10%', '1%', '0.1%', '0.01%', '0.001%'), minor_breaks=NULL, expand=expand_scale(mult=0.04)) +
#         scale_x_continuous(expand=expand_scale(mult=0.02)) +
#         scale_x_log10(breaks=c(50, 70, 100, 140, 200, 300, 450, 700, 1000, 1500, 3000, 5000, 10000), minor_breaks=NULL, expand=expand_scale(mult=0.04)) +
#         coord_cartesian(xlim=c(50, 1100), ylim=c(1.5e-4, 1)) +
         scale_x_continuous(breaks=integer_breaks(n=7)) +
         xlab('Number of active streams') +
         ylab('Duration (ms)') +
#         ggtitle(title) +
#         scale_colour_discrete(name="Scheduler", breaks=c("UDP", "TCP")) +
#         scale_linetype_manual(name="Scheduler", values=c("dashed", "twodash")) +
         theme_bw() +
         theme(legend.position = c(1.4, 1), legend.justification=c(1.001, 1.001), legend.background = element_rect(linetype="solid", colour="black", size=0.2))

ggsave(output, graph, width=4, height=3)
