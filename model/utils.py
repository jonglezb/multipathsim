from __future__ import division

from collections import namedtuple
import copy
from itertools import groupby
import math

from constants import EPSILON
import scheduling

# All times (speed, delay, release time) are in arbitrary time unit, but
# must be consistent with the size unit.
# A good convention for small examples can be:
# - delay and release time in ms
# - size in bytes
# - speed in bytes/ms i.e. KB/s
Path = namedtuple("Path", ["id", "speed", "delay"])

class Stream(object):
    """Release time: date at which this stream is created and available

    Size: total size of this stream

    Release after: (optional) ID of a stream on which we depend. We are
    only available after the stream has finished (all its data has been
    fully received).  If our release time is greater than this finishing
    date, we will still wait until the release time.

    Release delay: (optional) when the stream on which we depend has
    finished, apply the given delay before we become available.  This
    could model computation, web page parsing, database or disk lookup...
    """
    __slots__ = ["id", "release_time", "size", "release_after", "release_delay"]

    def __init__(self, id, release_time, size, release_after=None, release_delay=None):
        self.id = id
        self.release_time = release_time
        assert(size > 0)
        self.size = size
        assert(release_after == None or isinstance(release_after, int))
        self.release_after = release_after
        self.release_delay = release_delay

    def __str__(self):
        attrs = ["{}={}".format(attr, getattr(self, attr)) for attr in self.__slots__]
        return '{}({})'.format(type(self).__name__, ", ".join(attrs))

# A solution is a list of allocation.
# The stream ID is always the "global" ID from the original problem.
Allocation = namedtuple("Allocation", ["stream_id", "path_id", "time_start", "time_stop"])


class ProblemInstance(object):

    def __init__(self, data_file=None, streams=None, paths=None):
        if data_file == None:
            # Build problem instance directly with the given streams and paths
            self.nstreams = len(streams)
            self.streams = streams
            self.npaths = len(paths)
            self.paths = paths
            return
        f = open(data_file)
        npaths, nstreams = f.readline().split()[:2]
        self.npaths = int(npaths)
        self.nstreams = int(nstreams)

        f.readline()
        self.paths = []
        self.streams = []
        for p in range(self.npaths):
            self.paths.append(Path(p, *[float(elt) for elt in (f.readline()[:-1]).split()]))
        f.readline()
        for s in range(self.nstreams):
            line = f.readline()[:-1].split()
            if len(line) >= 4:
                stream = Stream(s, float(line[0]), float(line[1]), int(line[2]), float(line[3]))
            elif len(line) >= 3:
                stream = Stream(s, float(line[0]), float(line[1]), int(line[2]))
            else:
                stream = Stream(s, float(line[0]), float(line[1]))
            self.streams.append(stream)

    def deepcopy(self):
        """Returns a deep copy of the instance.  The only mutable members are
        streams."""
        streams = copy.deepcopy(self.streams)
        paths = list(self.paths)
        return ProblemInstance(None, streams, paths)

    def lower_bound(self, stream=None, delay=True):
        """Compute a lower bound on performance:

        - with a stream and delay=True, computes a lower bound on its completion date;

        - without a stream and delay=True, computes a lower bound on the total makespan;

        - with a stream and delay=False, computes a lower bound on the
          date at which it will stop transmitting;

        - without a stream and delay=False, computes a lower bound on the
          date at which all streams will stop transmitting.
        """
        if len(self.streams) == 0:
            return 0
        total_size = sum([stream.size for stream in self.streams])
        total_speed = sum([path.speed for path in self.paths])
        min_delay = min([path.delay for path in self.paths])
        if not delay:
            min_delay = 0
        # Minimum makespan
        if stream == None:
            release_time_f = lambda s: s.release_time
            # Iterate on release dates and accumulate work to do, assuming
            # that we always run at total_speed (all paths in parallel).
            # Then look at the remaining work at the end.
            work_remaining = 0
            current_time = 0
            for (release_time, streams) in groupby(sorted(self.streams, key=release_time_f), release_time_f):
                if release_time != current_time:
                    work_remaining -= total_speed * (release_time - current_time)
                    work_remaining = max(work_remaining, 0)
                work_remaining += sum(s.size for s in streams)
                current_time = release_time
            finish_date = current_time + math.floor(work_remaining / total_speed)
            #print("lower_bound(stream={}, delay={}) = {}".format(stream, delay, finish_date + min_delay))
            return finish_date + min_delay
        else:
            # Lower bound on completion time
            return stream.release_time + min_delay + math.floor(stream.size / total_speed)

    def upper_bound(self, stream=None):
        # The worst case for a stream is the makespan, so we just estimate
        # the maximum makespan.
        total_size = sum([stream.size for stream in self.streams])
        total_speed = sum([path.speed for path in self.paths])
        max_delay = max([path.delay for path in self.paths])
        # For each stream, with its release time, what is the best
        # possible completion time
        individual = [stream.release_time + math.ceil(stream.size / total_speed) + max_delay for stream in self.streams]
        return max(max(individual), max_delay + math.ceil(total_size / total_speed))


def normalize_solution(solution):
    """Normalize a solution by merging adjacent allocations"""
    res = list()
    for (stream_id, path_id), allocs in groupby(sorted(solution, key=lambda alloc: (alloc.stream_id, alloc.path_id, alloc.time_start)),
                                     key=lambda alloc: (alloc.stream_id, alloc.path_id)):
        allocs = list(allocs)
        current_alloc = allocs[0]
        for alloc in allocs[1:]:
            if abs(current_alloc.time_stop - alloc.time_start) > EPSILON:
                res.append(current_alloc)
                current_alloc = alloc
            else:
                # Merge
                current_alloc = Allocation(stream_id, path_id, current_alloc.time_start, alloc.time_stop)
        # Add last allocation
        res.append(current_alloc)
    return res


def compute_end_date(instance, solution):
    """Compute the date at which all streams are fully received"""
    end_date = 0
    for stream_id in range(instance.nstreams):
        completion_date = max([alloc.time_stop + instance.paths[alloc.path_id].delay
                               for alloc in solution
                               if alloc.stream_id == stream_id])
        end_date = max(end_date, completion_date)
    return end_date

def compute_completion_times(instance, solution):
    """Completion time, or "response time", is the time spent in the system,
    i.e. between the release date and the date at which data is completely received."""
    res = []
    for stream_id in range(instance.nstreams):
        completion_date = max([alloc.time_stop + instance.paths[alloc.path_id].delay
                               for alloc in solution
                               if alloc.stream_id == stream_id])
        res.append(completion_date - instance.streams[stream_id].release_time)
    return res

def average_completion_time(instance, solution):
    completion_times = compute_completion_times(instance, solution)
    return sum(completion_times) / len(completion_times)

def max_completion_time(instance, solution):
    completion_times = compute_completion_times(instance, solution)
    return max(completion_times)


def compute_stretches(instance, solution):
    """The stretch of a stream is defined as (response time) / (optimal
    response time), where this optimal is computed as if the stream was
    alone (using ECF).
    """
    completion_times = compute_completion_times(instance, solution)
    # TODO: move this ECF computation code somewhere else...
    policy = scheduling.ExactSrptEcfPolicy()
    ecf_times = [policy.optimal_completion_time(stream.size, instance.paths) for stream in instance.streams]
    return [actual / optimal for (actual, optimal) in zip(completion_times, ecf_times)]
    
def average_stretch(instance, solution):
    stretches = compute_stretches(instance, solution)
    return sum(stretches) / len(stretches)

def max_stretch(instance, solution):
    stretches = compute_stretches(instance, solution)
    return max(stretches)


def is_work_conserving(instance, solution):
    """Check whether the solution always keeps all paths busy, except possible
    at the end because of ECF. For this, we compare:

    1) the total time taken in the solution
    2) the total time in an equivalent problem with a single stream whose
       size is equal to the same of the sizes in the original problem

    We assume that some streams are available at time 0, and that there is
    always work to do.
    """
    end_date = compute_end_date(instance, solution)
    total_size = sum(stream.size for stream in instance.streams)
    policy = scheduling.ExactSrptEcfPolicy()
    single_stream_end_date = policy.optimal_completion_time(total_size, instance.paths)
    return abs(end_date - single_stream_end_date) < EPSILON


def print_solution(solution):
    print("# stream time_start time_stop path")
    for allocation in sorted(solution, key=lambda alloc: (alloc.stream_id, alloc.time_start)):
        print("{} {} {} {}".format(allocation.stream_id,
                                   allocation.time_start,
                                   allocation.time_stop,
                                   allocation.path_id))

def print_completion_times(instance, solution):
    print("# ordered_completion_times")
    completion_times = compute_completion_times(instance, solution)
    print(" ".join(["{:.2f}".format(c) for c in sorted(completion_times)]))
    print("# average_completion_time")
    print("{:.2f}".format(sum(completion_times) / len(completion_times)))
