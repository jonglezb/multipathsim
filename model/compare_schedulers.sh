#!/bin/bash

SCHEDULERS="FCFS-minRTT SRPT-minRTT SRPT-ECF DRR-ECF"

INSTANCES="traces/*/*.txt"

for instance in $INSTANCES
do
    echo "[*] Working on $instance"
    instance_name=$(basename "$instance")
    input_dir=$(dirname "$instance")
    output_dir=results/${input_dir#traces/}
    mkdir -p "$output_dir"
    output_file_1="$output_dir"/"${instance_name/.txt}_completiontime.csv"
    output_file_2="$output_dir"/"${instance_name/.txt}_occupation.csv"
    output_graph_1="$output_dir"/"${instance_name/.txt}_completiontime.pdf"
    output_graph_2="$output_dir"/"${instance_name/.txt}_occupation.pdf"
    output_graph_3="$output_dir"/"${instance_name/.txt}_occupation_histo.pdf"
    echo "Scheduler,CompletionTime" > "$output_file_1"
    echo "Scheduler,Date,Occupation" > "$output_file_2"
    for scheduler in $SCHEDULERS
    do
	echo "[**] Trying scheduler $scheduler"
	./solve_policy.py "$scheduler" "$instance" headless | while read line
	do
	    case "$line" in
		occupation:*)
		    echo "$scheduler","${line#occupation: }" >> "$output_file_2"
		    ;;
		*)
		    echo "$scheduler","$line" >> "$output_file_1"
		    ;;
	    esac
	done
    done
    echo "[**] Generating graphs"
    ./plot_responsetime_ccdf.R "$output_file_1" "$instance_name" "$output_graph_1"
    ./plot_occupation_time.R "$output_file_2" "$instance_name" "$output_graph_2"
    ./plot_occupation_histogram.R "$output_file_2" "$instance_name" "$output_graph_3"
done
