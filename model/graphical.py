from collections import defaultdict

import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib import ticker
from matplotlib.collections import PatchCollection
from matplotlib.lines import Line2D
from matplotlib.patches import Rectangle

# Display margin on each side (multiplicative, i.e. 0.05 = 5%)
MARGIN = 0.05

def pos_path_y(instance, path_id):
    """Returns the y-axis position of path [path_id] as a couple (ymin, ymax)"""
    path = instance.paths[path_id]
    # Partial sum, also works for path_id == 0 (empty list)
    ymin = sum(p.speed for p in instance.paths[:path_id])
    ymax = ymin + path.speed
    return (ymin, ymax)

def pos_path_y_center(instance, path_id):
    ymin, ymax = pos_path_y(instance, path_id)
    return (ymin + ymax) / 2

def path_id_from_y(instance, y):
    """Reverse function: from a y position, return the path ID"""
    speed = 0
    for p in instance.paths:
        speed += p.speed
        if y < speed:
            return p.id

def path_from_y(instance, y):
    """Reverse function: from a y position, return the path ID"""
    speed = 0
    for p in instance.paths:
        speed += p.speed
        if y < speed:
            return p

def display_solution(instance, solution, policy_name, instance_name):
    """Display the solution graphically, both from the point of view of the
    sender and from the point of view of the receiver."""
    total_speed = sum(path.speed for path in instance.paths)
    # Setup display
    fig, (ax_sender, ax_receiver) = plt.subplots(nrows=2, ncols=1, sharex=True, sharey=False)
    colormap = cm.get_cmap("tab20", instance.nstreams)
    # Add back X labels to top graph
    ax_sender.tick_params(reset=True)
    # Titles
    ax_sender.set_title("Schedule as seen by sender", fontsize=11)
    ax_receiver.set_title("Schedule as seen by receiver", fontsize=11)
    #fig.suptitle("Policy: {}\nInstance: {}".format(policy_name, instance_name),
    #             fontsize=14)
    # Slightly increase the space between subplots
    fig.subplots_adjust(hspace=0.3)
    # Limits and layout
    for ax in (ax_sender, ax_receiver):
        ax.xaxis.set_label_text("Time")
        #ax.set_xlim(0, instance.upper_bound())
        #ax.set_xlim(-1.5, 38)
        ax.set_ylim(- total_speed * MARGIN, total_speed * (1 + MARGIN))
        # Center Y labels
        ax.yaxis.set_major_locator(ticker.FixedLocator([pos_path_y_center(instance, path.id) for path in instance.paths]))
        ax.yaxis.set_major_formatter(ticker.FuncFormatter(lambda y, pos:
                                                          'Path {0.id}\nSpeed = {0.speed}\nDelay = {0.delay}'.format(path_from_y(instance, y))))
        # Put path 0 on top (more readable)
        ax.invert_yaxis()
    # Add legend only for small number of streams
    if len(instance.streams) <= 5:
        legend_items = []
        legend_labels = []
        for stream in instance.streams:
            legend_items.append(Line2D([0], [0], linestyle="none", marker="s", alpha=0.8, markersize=10, markerfacecolor=colormap.colors[stream.id]))
            legend_labels.append("Stream {}".format(stream.id))
        #ax_sender.legend(legend_items, legend_labels, numpoints=1, fontsize=8, loc="lower right")
        #ax_receiver.legend(legend_items, legend_labels, numpoints=1, fontsize=8, loc="lower left")
    # Map from stream ID to a list of rectangles
    sender_rectangles_by_stream = defaultdict(list)
    sender_lines_by_stream = defaultdict(list)
    receiver_rectangles_by_stream = defaultdict(list)
    receiver_lines_by_stream = defaultdict(list)
    for stream in instance.streams:
        # On the sending side, add a line (0-width rectangle) to display
        # the starting date of the stream.
        sender_lines_by_stream[stream.id].append(Rectangle((stream.release_time, - total_speed * MARGIN),
                                                           0, total_speed * (1 + 2*MARGIN)))
    for allocation in solution:
        path = instance.paths[allocation.path_id]
        x = allocation.time_start
        y, _ = pos_path_y(instance, allocation.path_id)
        sizex = allocation.time_stop - allocation.time_start
        sizey = path.speed
        sender_rectangles_by_stream[allocation.stream_id].append(Rectangle((x, y), sizex, sizey))
        receiver_rectangles_by_stream[allocation.stream_id].append(Rectangle((x + path.delay, y), sizex, sizey))
        # On the sender side, add a line (0-width rectangle) to display
        # the completion date for this rectangle.
        #completion_date = allocation.time_stop + path.delay
        #sender_lines_by_stream[allocation.stream_id].append(Rectangle((completion_date, y), 0, sizey))
    for stream_id in range(instance.nstreams):
        # Sender rectangles
        pc = PatchCollection(sender_rectangles_by_stream[stream_id],
                             facecolor=colormap.colors[stream_id],
                             alpha=0.5,
                             edgecolor='black')
        ax_sender.add_collection(pc)
        # Sender lines
        pc = PatchCollection(sender_lines_by_stream[stream_id],
                             alpha=0.8,
                             linestyle=':',
                             edgecolor=colormap.colors[stream_id])
        ax_sender.add_collection(pc)
        # Receiver rectangles
        pc = PatchCollection(receiver_rectangles_by_stream[stream_id],
                             facecolor=colormap.colors[stream_id],
                             alpha=0.5,
                             edgecolor='black')
        ax_receiver.add_collection(pc)
        # Receiver lines
        pc = PatchCollection(receiver_lines_by_stream[stream_id],
                             alpha=0.8,
                             linestyle=':',
                             edgecolor=colormap.colors[stream_id])
        ax_receiver.add_collection(pc)
    # Comment this when using set_xlim() above
    plt.autoscale(axis='x')
    plt.show()
