#!/usr/bin/env python3

from itertools import groupby
import math

from Numberjack import *

from utils import Path, Stream, Allocation, ProblemInstance, print_solution
from graphical import display_solution
import discrete_event
import scheduling
import path_breaks_model
import stream_breaks_model

"""
Optimal solution for the multi-stream multi-path scheduling problem.
The idea is to allow a given number of "breaks", that is, instants at
which a stream or path is allowed to change its allocation.
"""

def work_done_by_stream(jsp, solution):
    work_by_stream = [0] * jsp.nstreams
    for allocation in solution:
        duration = allocation.time_stop - allocation.time_start
        path = jsp.paths[allocation.path_id]
        work_by_stream[allocation.stream_id] += duration * path.speed
    return work_by_stream

def get_completion_dates(jsp, solution):
    """Returns the completion date for each stream in the solution (including path delay)."""
    dates = [0] * jsp.nstreams
    for path_id, path in enumerate(jsp.paths):
        for stream_id in range(jsp.nstreams):
            subsolution = list(filter(lambda alloc: alloc.stream_id == stream_id and alloc.path_id == path_id, solution))
            if len(subsolution) == 0:
                continue
            latest = max(subsolution, key=lambda alloc: alloc.time_stop)
            dates[stream_id] = max(dates[stream_id], latest.time_stop + path.delay)
    return dates

def get_flow_times(jsp, solution):
    """Returns the "flow time" for each stream in the solution, that is,
    completion date minus release date."""
    completion_dates = get_completion_dates(jsp, solution)
    return [completion_dates[stream_id] - stream.release_time for stream_id, stream in enumerate(jsp.streams)]

def finish_date(solution):
    """Returns the date at which streams stop transmitting in this solution
    (does not account for subsequent path delays!)
    """
    return max(solution, key=lambda alloc: alloc.time_stop).time_stop

def shift_streams(streams, timedelta):
    """Given a list of streams, return new streams whose release times are
    shifted by the given time delta.  If positive, the new streams will
    start later; if negative, the new streams will start earlier.
    """
    return [Stream(id=s.id, release_time=s.release_time + timedelta, size=s.size) for s in streams]

def shift_solution(solution, timedelta):
    """Given a solution, return a new solution whose dates are all shifted by
    the given time delta.
    """
    return [Allocation(stream_id=alloc.stream_id,
                       path_id=alloc.path_id,
                       time_start=alloc.time_start + timedelta,
                       time_stop=alloc.time_stop + timedelta)
            for alloc in solution]

def solve_model_iterative(param, jsp, continuous):
    """Iterate to try solving disjoint problems: if we finish scheduling some
    tasks before the release date of later tasks, the problems are
    disjoint and can be solved independently.
    """
    if param['model'] == 'path_breaks':
        module = path_breaks_model
    elif param['model'] == 'stream_breaks':
        module = stream_breaks_model
    else:
        return
    release_time_f = lambda s: s.release_time
    start_time = 0
    global_solution = []
    solution = None
    subproblem_streams = []
    groups = [(release_time, list(streams)) for (release_time, streams) in groupby(sorted(jsp.streams, key=release_time_f), release_time_f)]
    # Add guard at the end (to ease iteration)
    groups.append((None, None))
    for (release_time1, streams), (release_time2, _) in zip(groups, groups[1:]):
        # Build new subproblem
        subproblem_streams.extend(shift_streams(streams, -start_time))
        subproblem = ProblemInstance(data_file=None, streams=subproblem_streams, paths=jsp.paths)
        # Check if we have a reasonable chance to finish this work before
        # the release date of the next streams.
        lower_bound = subproblem.lower_bound(stream=None, delay=False)
        if release_time2 != None and start_time + lower_bound >= release_time2:
            # Not a chance
            #print("Best estimate for finishing is {}, next arrival at {}".format(start_time + lower_bound, release_time2))
            continue
        if param['verbose'] >= 1:
            print("Solving new subproblem at time {} with {} stream(s): {}".format(start_time, len(subproblem_streams), subproblem_streams))
        model, model_data = module.get_model(subproblem, param['nbreaks'], continuous)
        if param['verbose'] >= 2:
            print(model)
        solver = model.load(param['solver'])
        solver.setVerbosity(param['verbose'])
        solver.setTimeLimit(param['tcutoff'])
        solver.solve()
        # Extract solution
        solution = module.get_solution(param, subproblem, param['nbreaks'], model_data)
        # Check solution: will it finish before the next release time?
        if release_time2 == None or finish_date(solution) <= release_time2:
            if param['verbose'] >= 1:
                print("Finished solving disjoint subproblem with {} streams".format(len(subproblem_streams)))
            # Combine current solution to the subproblem to the global solution
            global_solution.extend(shift_solution(solution, start_time))
            # Reset subproblem
            subproblem_streams = []
            start_time = release_time2
    return global_solution


def solve_policy(param, jsp):
    """Solve problem with the given scheduling policy."""
    if param['model'] not in scheduling.policies.keys():
        return []
    Policy = scheduling.policies[param['model']]
    policy = Policy()
    return discrete_event.run_simulation(policy, jsp)


def solve(param):
    # Build model
    jsp = ProblemInstance(param['data'])

    if param['continuous'] == 'yes':
        continuous = True
    elif param['continuous'] == 'no':
        continuous = False
    elif param['continuous'] == 'auto':
        continuous = (param['solver'] in ("Gurobi", "CPLEX", "SCIP"))
    else:
        print("Invalid continuous choice, valid values: 'yes', 'no', 'auto'")
        exit(1)

    if param['model'] in ('path_breaks', 'stream_breaks'):
        solution = solve_model_iterative(param, jsp, continuous)
    elif param['model'] in scheduling.policies:
        solution = solve_policy(param, jsp)
    else:
        allowed_models = ['path_breaks', 'stream_breaks']
        allowed_models.extend(scheduling.policies)
        print("Invalid model, valid values: {}".format(", ".join(allowed_models)))
        exit(1)


    print("Input problem:")
    print("Size of each stream:", [s.size for s in jsp.streams])
    print("Stream release dates:", [s.release_time for s in jsp.streams])
    print("Capacity of each path:", [p.speed for p in jsp.paths])
    print("Delay of each path:", [p.delay for p in jsp.paths])
    print()
    print("Output:")
    work_done = work_done_by_stream(jsp, solution)
    print("Work done on each stream:", work_done)
    print("Completion dates:", get_completion_dates(jsp, solution))
    flow_times = get_flow_times(jsp, solution)
    print("Flow times:", flow_times)
    print("Score:", sum(flow_times))
    #print("Break times:", [[b.get_value() for b in s] for s in break_times])
    print()
    print("Raw solution:")
    print_solution(solution)
    if param['display'] == 'yes':
        display_solution(jsp, solution)


default = {'solver': 'Mistral2', 'data': 'optimal/multipath.txt', 'model': 'path_breaks', 'display': 'no', 'continuous': 'no', 'verbose': 1, 'tcutoff': 30, 'nbreaks': 3}


if __name__ == '__main__':
    param = input(default)
    solve(param)
