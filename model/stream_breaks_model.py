from Numberjack import *

from utils import Path, Stream, Allocation


def get_model(jsp, nbreaks, continuous):
    """
    Build a model in which all streams have the given number of allowed
    breaks.
    """
    ###############################################
    ##############      Model        ##############
    ###############################################
    lb = jsp.lower_bound()
    ub = jsp.upper_bound()

    # Instants at which a given stream will change its path allocation.
    # Break 0 corresponds to the first path allocation of a stream (start of transmission).
    # Break n corresponds to the end of transmission for a stream.
    if continuous:
        break_times = Matrix([[Variable(float(stream.release_time), float(jsp.upper_bound(stream)), 't_{}_{}'.format(stream_id, i)) for i in range(nbreaks+1)] for stream_id, stream in enumerate(jsp.streams)])
    else:
        break_times = Matrix([[Variable(stream.release_time, jsp.upper_bound(stream), 't_{}_{}'.format(stream_id, i)) for i in range(nbreaks+1)] for stream_id, stream in enumerate(jsp.streams)])
    # Completion time for each stream: at which date the stream will finish.
    # The lower bound accounts for the release date.
    if continuous:
        completion_dates = VarArray([Variable(float(jsp.lower_bound(stream)), float(jsp.upper_bound(stream)), 'C_{}'.format(stream_id)) for stream_id, stream in enumerate(jsp.streams)])
    else:
        completion_dates = VarArray([Variable(jsp.lower_bound(stream), jsp.upper_bound(stream), 'C_{}'.format(stream_id)) for stream_id, stream in enumerate(jsp.streams)])
    # Decision variable: for each stream, path allocation between two breaks.
    # x_stream_path_i == 1 means that [stream] is using [path] between breaks [i] and [i+1]
    v = [[[Variable('x_{}_{}_{}'.format(stream, path, i)) for i in range(nbreaks)] for path in range(jsp.npaths)] for stream in range(jsp.nstreams)]

    model = Model(
        # Ordering on break times
        [[break_times[stream][i] <= break_times[stream][i+1] for i in range(nbreaks)] for stream in range(jsp.nstreams)],
        # Don't allow two streams with overlapping intervals on the same path.
        # We iterate on each (distinct) pairs of streams and pairs of intervals...
        # The logical formula is "intervals overlap  ==>  x1 + x2 <= 1"
        [[[[[(break_times[s2][j] >= break_times[s1][i+1]) | (break_times[s2][j+1] <= break_times[s1][i]) | (v[s1][p][i] + v[s2][p][j] <= 1)  for s2 in range(s1)] for s1 in range(jsp.nstreams)] for j in range(nbreaks)] for i in range(nbreaks)] for p in range(jsp.npaths)],
        # Ensure enough work is done on each stream
        [sum(sum(v[stream_id][path_id][i] * (break_times[stream_id][i+1] - break_times[stream_id][i]) * path.speed for path_id, path in enumerate(jsp.paths)) for i in range(nbreaks)) >= stream.size for stream_id, stream in enumerate(jsp.streams)],
        # Completion time: for each interval [t_i, t_{i+1}], if it is
        # using path p, then it constrains the completion time to be
        # greater than t_{i+1} + p.delay.
        # We use again the equivalence "A => B" with "not(A) | B"
        [[[(v[stream][path_id][i] == 0) | (completion_dates[stream] >= break_times[stream][i+1] + path.delay) for i in range(nbreaks)] for path_id, path in enumerate(jsp.paths)] for stream in range(jsp.nstreams)],
        # Optimise sum of "flow times", which is completion date - release date
        Minimise(Sum([completion_dates[stream_id] - stream.release_time for stream_id, stream in enumerate(jsp.streams)]))
    )
    return model, (completion_dates, break_times, v)


def get_solution(param, jsp, nbreaks, model_data):
    (completion_dates, break_times, variables) = model_data
    solution = []
    for stream_id, stream in enumerate(jsp.streams):
        for path_id, path in enumerate(jsp.paths):
            for i in range(nbreaks):
                if variables[stream_id][path_id][i].get_value():
                    start = break_times[stream_id][i].get_value()
                    stop = break_times[stream_id][i+1].get_value()
                    if start == stop:
                        continue
                    # This stream.id (global ID in the original problem)
                    # might be different from stream_id (local ID in the
                    # subproblem)!
                    allocation = Allocation(stream.id, path_id, start, stop)
                    solution.append(allocation)
    return solution
