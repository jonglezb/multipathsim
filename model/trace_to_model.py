#!/usr/bin/env python3

"""
Analyse a trace from webpagetest.org and generate an instance for simulation

"""

from collections import defaultdict
import csv
from datetime import datetime
import json
import os
from pprint import pprint
import sys


def connections_from_json(data):
    # Only take run 3 to ensure CDN caching
    requests = data['data']['runs']['3']['firstView']['requests']
    # Build a list of requests per connection
    connections = defaultdict(list)
    for req in requests:
        id = req["index"]
        name = req["id"]
        host = req["host"]
        ip = req["ip_addr"]
        socket = req["socket"]
        connections[socket].append(req)
    # Debug
    pprint({key: (len(reqs), {req["host"] for req in reqs})  for key, reqs in connections.items()})
    # Check consistency: exactly one connection opening per connection
    for conn, reqs in connections.items():
        openings = [req for req in reqs if req["connect_ms"] != -1]
        assert(len(openings) == 1)
    return connections

def build_paths(capacity, latency):
    """
    Returns a list of paths from a given capacity and latency.

    Capacity is given in Kbit/s, and latency in ms.

    We return each path as a pair (speed in KB/s, latency in ms)."""
    paths = []
    paths.append((capacity * 0.8 / 8, latency * 0.8))
    paths.append((capacity * 0.4 / 8, latency * 1.8))
    return paths

def convert_from_json(filename, output_dir):
    with open(filename) as f:
        data = json.load(f)
    connections = connections_from_json(data)
    conn_latency = dict()
    # Determine latency for each connection from the trace
    for conn, reqs in connections.items():
        min_latency_ttfb = min(req['ttfb_ms'] for req in reqs)
        min_latency_connect = min(req['connect_ms'] for req in reqs if req['connect_ms'] != -1)
        min_latency = min(min_latency_ttfb, min_latency_connect)
        conn_latency[conn] = min_latency
    # Read configured max throughput from the JSON
    capacity = data["data"]["bwDown"]
    # Create directory for the experiment
    date = datetime.fromtimestamp(data["data"]["runs"]["3"]["firstView"]["date"]).isoformat()
    expe_id = data["data"]["id"]
    website = data["data"]["testUrl"]
    if website.startswith("https://"):
        website = website[8:]
    elif website.startswith("http://"):
        website = website[7:]
    website = website.replace("/", "^")
    location = data["data"]["location"].replace(':', '-')
    connectivity = data["data"]["connectivity"]
    expe_dir = "{}_{}_{}_{}_{}".format(date, expe_id, website, location, connectivity)
    os.makedirs(os.path.join(output_dir, expe_dir)) # Fail if already exists
    # Write down result
    for conn, reqs in connections.items():
        # Discard connections with 1 or 2 connections
        if len(reqs) <= 2:
            continue
        paths = build_paths(capacity, conn_latency[conn])
        hosts = ','.join(sorted({req["host"] for req in reqs}))
        result_name = "{}-{}.txt".format(len(reqs), hosts)
        out_file = os.path.join(output_dir, expe_dir, result_name)
        with open(out_file, "w") as ff:
            ff.write("{} {}\n".format(len(paths), len(reqs)))
            ff.write("# paths: speed, delay in ms\n")
            for path in paths:
                ff.write("{} {}\n".format(path[0], path[1]))
            ff.write("# streams: release time, size\n")
            for req in reqs:
                # Compute size as header size + body size
                # See https://github.com/WPO-Foundation/webpagetest/issues/1347
                # This also avoids issues with the response body is empty (size would be 0)
                response_size = req['bytesIn'] + len('\r\n'.join(req["headers"]["response"]))
                ff.write("{} {}\n".format(req['load_start'], response_size))

def main():
    json_input = sys.argv[1]
    output_dir = sys.argv[2]
    convert_from_json(json_input, output_dir)
    

if __name__ == '__main__':
    main()
