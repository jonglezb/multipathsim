from Numberjack import *

from utils import Path, Stream, Allocation


def get_model(jsp, nbreaks, continuous):
    """Build a model in which all paths have the given number of allowed
    breaks.  This makes it easier to ensure that only one stream is using
    a path at any given time, but makes the objective function more
    difficult to express.
    """
    ###############################################
    ##############      Model        ##############
    ###############################################
    lb = jsp.lower_bound()
    ub = jsp.upper_bound()

    # Instants at which a given path will change its stream allocation.
    # Break 0 corresponds to the start of the first stream allocation.
    # Break n corresponds to the end of transmission on a path (but the
    # propagation time still needs to be taken into account).
    if continuous:
        break_times = Matrix([[Variable(0., float(ub), 'T_p{}_{}'.format(path_id, i)) for i in range(nbreaks+1)] for path_id in range(jsp.npaths)])
    else:
        break_times = Matrix([[Variable(0, ub, 'T_p{}_{}'.format(path_id, i)) for i in range(nbreaks+1)] for path_id in range(jsp.npaths)])
    # Completion time for each stream: at which date the stream will finish.
    # The lower bound accounts for the release date.
    if continuous:
        completion_dates = VarArray([Variable(float(jsp.lower_bound(stream)), float(jsp.upper_bound(stream)), 'C_{}'.format(stream_id)) for stream_id, stream in enumerate(jsp.streams)])
    else:
        completion_dates = VarArray([Variable(jsp.lower_bound(stream), jsp.upper_bound(stream), 'C_{}'.format(stream_id)) for stream_id, stream in enumerate(jsp.streams)])
    # Decision variable: for each path, stream allocation between two breaks.
    # S_path_i is the ID of the stream is using [path] between breaks [i] and [i+1].
    # A value equal to nstreams means that no stream is allocated in the given interval.
    v = Matrix([[Variable(0, jsp.nstreams, 'S_p{}_{}'.format(path, i)) for i in range(nbreaks)] for path in range(jsp.npaths)])

    model = Model(
        # Ordering on break times
        [[break_times[path_id][i] <= break_times[path_id][i+1] for i in range(nbreaks)] for path_id in range(jsp.npaths)],
        # Release dates
        [[[(v[path_id][i] != stream_id) | (break_times[path_id][i] >= stream.release_time) for i in range(nbreaks)] for path_id, path in enumerate(jsp.paths)] for stream_id, stream in enumerate(jsp.streams)],
        # Ensure enough work is done on each stream.
        [sum(sum((break_times[path_id][i+1] - break_times[path_id][i]) * path.speed * (v[path_id][i] == stream_id) for i in range(nbreaks)) for path_id, path in enumerate(jsp.paths)) >= stream.size for stream_id, stream in enumerate(jsp.streams)],
        # Optimise sum of "flow times", which is completion date - release date
        Minimise(Sum([completion_dates[stream_id] - stream.release_time for stream_id, stream in enumerate(jsp.streams)]))
    )
    if jsp.nstreams == 1:
        model.add([[completion_dates[0] >= break_times[path_id][i+1] + path.delay for i in range(nbreaks)] for path_id, path in enumerate(jsp.paths)])
    else:
        # Completion time: for a given path, the stream being allocated on
        # interval [T_i, T_{i+1}] is the one whose completion time is
        # constrained.  The constraint is C_stream >= T_{i+1} + path.delay
        model.add([[Element(completion_dates, v[path_id][i]) >= break_times[path_id][i+1] + path.delay for i in range(nbreaks)] for path_id, path in enumerate(jsp.paths)])
    return model, (completion_dates, break_times, v)


def get_solution(param, jsp, nbreaks, model_data):
    (completion_dates, break_times, variables) = model_data
    solution = []
    for path_id, path in enumerate(jsp.paths):
        for i in range(nbreaks):
            stream_id = variables[path_id][i].get_value()
            # Empty allocation
            if stream_id == jsp.nstreams:
                continue
            start = break_times[path_id][i].get_value()
            stop = break_times[path_id][i+1].get_value()
            if start == stop:
                continue
            stream = jsp.streams[stream_id]
            # This stream.id (global ID in the original problem) might be
            # different from stream_id (local ID in the subproblem)!
            allocation = Allocation(stream.id, path_id, start, stop)
            solution.append(allocation)
    return solution
