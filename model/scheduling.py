from collections import defaultdict
import math

from constants import EPSILON

class SchedulingPolicy(object):

    def __init__(self, *args, **kwargs):
        pass

    def account_for_data_sent(self, stream, path, nb_bytes, now):
        """Called by the scheduler to inform us: this much data was sent from the
        given stream to the given path since the previous scheduling
        decision.
        """
        pass

    def remove_stream(self, stream, now):
        """Called by the scheduler to inform us when a stream has no more data to
        send.
        """
        pass

    def schedule(active_streams, active_paths, all_paths):
        """The schedule method must return either of:

        - a triple (stream, path, duration), which means that [stream]
          should be allocated to [path] for the given [duration].  The
          allocation might be cut short before the expected [duration] if
          an expected event happens in-between, for instance a new stream
          arrival.  However, the allocation will never exceed [duration].

        - a triple (stream, path, None) if the allocation has no
          limitation on duration.  It will stop whenever the stream is
          exhausted or whenever a new event happens.

        - None if the policy decides not to use any of the available paths.

        """
        raise NotImplemented

class FcfsMinrttPolicy(SchedulingPolicy):
    """Choose streams in-order, and assign them to the path with lowest RTT"""
    def schedule(self, active_streams, available_paths, *args, **kwargs):
        return active_streams[0], min(available_paths, key=lambda path: path.delay), None

class SmallestMinrttPolicy(SchedulingPolicy):
    """Choose smallest streams first, and assign them to the path with lowest RTT"""

    def schedule(self, active_streams, available_paths, *args, **kwargs):
        stream = min(active_streams, key=lambda s: s.remaining_size)
        path = min(available_paths, key=lambda p: p.delay)
        return stream, path, None


class SrptEcfPolicy(SchedulingPolicy):

    def path_completion_time(self, path, nb_bytes):
        return path.delay + nb_bytes / path.speed

    def schedule(self, active_streams, available_paths, all_paths, *args, **kwargs):
        ordered_streams = sorted(active_streams, key=lambda s: s.remaining_size)
        return self._schedule(ordered_streams, available_paths, all_paths)

    def _schedule(self, ordered_streams, available_paths, all_paths):
        # How much data (in bytes) already needs to be transmitted by
        # previous streams in the list (i.e. those with higher priority).
        # This is needed because even if the first stream does not want to
        # transmit, the completion time of the second stream needs to
        # account for the time to transmit the first stream entirely.
        scheduled_data = 0
        # This only works for two paths and needs generalisation for more paths.
        path1, path2 = sorted(all_paths, key=lambda p: p.delay)
        for stream in ordered_streams:
            scheduled_data += stream.remaining_size
            if self.path_completion_time(path1, scheduled_data) - path2.delay < EPSILON:
                if path1 in available_paths:
                    return stream, path1, None
                else: # Skip this stream (wait for path1 to become available)
                    continue
            if self.path_completion_time(path2, scheduled_data) - path1.delay < EPSILON:
                if path2 in available_paths:
                    return stream, path2, None
                else: # Skip this stream (wait for path2 to become available)
                    continue
            # Both paths are fine to use, at least for now.
            duration = (scheduled_data - path1.speed * (path2.delay - path1.delay)) / (path1.speed + path2.speed)
            return stream, available_paths[0], duration
        # No stream wants to transmit, return None


class ExactSrptEcfPolicy(SrptEcfPolicy):
    """Variant of SRCT in which we compute the exact estimated completion time
    and use it to order streams.  This improves stability because it
    avoids "inversion" between streams and better handles preemption from
    new streams.
    """
    def optimal_completion_time_twopaths(self, path1, path2, nb_bytes):
        """Optimal completion time to send [nb_bytes] bytes on two paths.
        """
        # Ensure path1 has lower delay than path2
        if path1.delay > path2.delay:
            path1, path2 = path2, path1
        return path1.delay + (nb_bytes + path2.speed * (path2.delay - path1.delay)) / (path1.speed + path2.speed)

    def optimal_completion_time(self, nb_bytes, all_paths):
        """Compute the optimal time needed to transfer [nb_bytes] using multiple
        paths."""
        # TODO: need recursive computation for > 2 paths
        path1, path2 = all_paths
        if self.path_completion_time(path1, nb_bytes) < path2.delay:
            return self.path_completion_time(path1, nb_bytes)
        if self.path_completion_time(path2, nb_bytes) < path1.delay:
            return self.path_completion_time(path2, nb_bytes)
        # Use both paths
        return self.optimal_completion_time_twopaths(path1, path2, nb_bytes)

    def remaining_completion_time(self, now, stream, all_paths):
        # TODO: memoize result, because sorted() can call us several time.
        remaining_data_completion_time = self.optimal_completion_time(stream.remaining_size, all_paths)
        # Account for propagation time of previous packets: look for finish date on each path.
        in_flight_latest_finish_date = max([path.in_flight_finish_date(stream.id) for path in all_paths])
        return max(remaining_data_completion_time,
                   in_flight_latest_finish_date - now)

    def schedule(self, active_streams, available_paths, all_paths, now, *args, **kwargs):
        ordered_streams = sorted(active_streams, key=lambda s: self.remaining_completion_time(now, s, all_paths))
        return self._schedule(ordered_streams, available_paths, all_paths)


class InflightSrptEcfPolicy(SchedulingPolicy):

    def schedule(self, active_streams, available_paths, all_paths, *args, **kwargs):
        pass


class DeficitRoundRobinEcfPolicy(ExactSrptEcfPolicy):
    """Serve streams with a Deficit Round-Robin algorithm, and compute which
    path(s) to use using ECF (accounting for the fact that all active
    streams share the capacity of the paths)

    """
    # Quantum in bytes
    QUANTUM = 200

    def __init__(self, *args, **kwargs):
        self.current_stream = None
        # Position of the current stream in the list of active streams
        # (which might change over time!).  We only use this when the
        # stream disappears from the list of active streams: in this case,
        # we switch to another stream that is at the same position.
        self.current_stream_pos = 0
        # For each stream, store its deficit counter in bytes, initialized at 0.
        self.stream_deficit = defaultdict(int)
        super().__init__(self, *args, **kwargs)

    def remove_stream(self, stream, now):
        if stream in self.stream_deficit:
            del self.stream_deficit[stream]

    def account_for_data_sent(self, stream, path, nb_bytes, now):
        """Called by the scheduler to inform us: this much data was sent from the
        given stream to the given path since the previous scheduling
        decision.
        """
        self.stream_deficit[stream] -= nb_bytes
        #print("Deficit now {} for stream".format(self.stream_deficit[stream]), stream)

    def schedule(self, active_streams, available_paths, all_paths, now, *args, **kwargs):
        # This only works for two paths and needs generalisation for more paths.
        path1, path2 = sorted(all_paths, key=lambda p: p.delay)
        nb_streams = len(active_streams)
        try:
            # Find our currently active stream
            self.current_stream_pos = active_streams.index(self.current_stream)
        except ValueError:
            # The current stream disappeared.  Switch to the one at the
            # same position, but be careful that the position is still
            # valid (reset it to 0 otherwise).
            if self.current_stream_pos >= nb_streams:
                self.current_stream_pos = 0
            self.current_stream = active_streams[self.current_stream_pos]
            # Give a quantum to the new current stream.  This also covers
            # initialization.
            self.stream_deficit[self.current_stream] += self.QUANTUM
        # At this point, self.current_stream and self.current_stream_pos
        # should always be consistent.

        # Iterate on streams, in case the next stream decides not to use
        # its slot.  But we need to stop once we have looped over all
        # streams.  l[n:] + l[:n] is useful to "cycle" a list so that
        # element n becomes the first element.
        for stream in active_streams[self.current_stream_pos:] + active_streams[:self.current_stream_pos]:
            # Newly active stream: give quantum and set as current.
            if stream != self.current_stream:
                self.stream_deficit[stream] += self.QUANTUM
                self.current_stream = stream
                self.current_stream_pos = active_streams.index(stream)
            # At this point, stream and self.current_stream are always equal.
            #print("Time", now, ", handling stream:", stream, "with deficit:", self.stream_deficit[stream])

            # Stream has depleted its deficit, switch to the next one.
            if self.stream_deficit[stream] < EPSILON:
                #print("Stream", stream, "depleted its deficit:", self.stream_deficit[stream])
                # Normal case, switch to next stream.
                if len(active_streams) > 1:
                    continue
                # Special case when we have a single active stream:
                # give a quantum and schedule it anyway
                else:
                    self.stream_deficit[stream] = self.QUANTUM
            if stream.remaining_size <= self.stream_deficit[stream]:
                # We can finish in one burst
                virtual_size = stream.remaining_size
            else:
                # We will be interleaved with all other streams, limited only
                # by our own size (that is, if another stream is larger than
                # us, we only need to account for it up to our own size)
                virtual_size = sum(min(other.remaining_size, stream.remaining_size) for other in active_streams)
            #completion_time = self.optimal_completion_time(virtual_size, all_paths)
            # Use ECF on the virtual size
            #print("-- virtual size:", virtual_size)
            #print("-- path1 completion time:", self.path_completion_time(path1, virtual_size))
            if self.path_completion_time(path1, virtual_size) - path2.delay < EPSILON:
                if path1 in available_paths:
                    #print("-->path1 only")
                    return stream, path1, self.stream_deficit[stream] / path1.speed
                else: # Skip this stream (wait for path1 to become available)
                    #print("-->wait for path1")
                    continue
            #print("-- path2 completion time:", self.path_completion_time(path2, virtual_size))
            if self.path_completion_time(path2, virtual_size) - path1.delay < EPSILON:
                if path2 in available_paths:
                    #print("-->path2 only")
                    return active_stream, path2, self.stream_deficit[stream] / path2.speed
                else: # Skip this stream (wait for path2 to become available)
                    #print("-->wait for path2")
                    continue
            # Both paths are fine to use, choose the highest-latency one
            # to leave space for other streams that may need the
            # low-latency one.
            path = max(available_paths, key=lambda p: p.delay)
            quantum_duration = self.stream_deficit[stream] / path.speed
            # Reduce duration if necessary to ensure simultaneous
            # completion on both paths.
            ecf_duration = (virtual_size - path1.speed * (path2.delay - path1.delay)) / (path1.speed + path2.speed)
            duration = min(quantum_duration, ecf_duration)
            #print("-- quantum duration {}, ecf duration {}".format(quantum_duration, ecf_duration))
            #if len(available_paths) > 1:
            #    print("-->both paths are fine, take high-latency")
            #else:
            #    print("-->only one path available")
            return stream, path, duration
        #print("nothing to transmit")
        # Return None (no stream wants to transmit anything on the available paths)


class StreamAwareECF(SchedulingPolicy):
    """SA-ECF scheduling strategy from: Rabitsch, P. Hurtig, and
    A. Brunstrom. A Stream-Aware Multipath QUIC Scheduler for
    Heterogeneous Paths. In Proceedings of the Workshop on the Evolution,
    Performance, and Interoperability of QUIC, EPIQ’18, pages 29–35, New
    York, NY, USA, 2018. ACM.

    It's basically Weighted Round Robin + ECF with some subtleties.

    Since we don't have weight, we simplify the algorithm a bit: each
    stream is given a fixed quantum (number of bytes) each time it is
    visited by round-robin and uses all of it.
    """
    # Number of bytes for each transmission
    QUANTUM = 1000
    # Hysteresis constant.  The paper gives no value...
    BETA = 1

    def __init__(self, *args, **kwargs):
        self.current_stream = None
        # Position of the current stream in the list of active streams
        # (which might change over time!).  We only use this when the
        # stream disappears from the list of active streams: in this case,
        # we switch to another stream that is at the same position.
        self.current_stream_pos = 0
        # Record the waiting state (0 or 1) for each stream
        self.waiting = defaultdict(int)
        super().__init__(self, *args, **kwargs)

    def remove_stream(self, stream, now):
        if stream in self.waiting:
            del self.waiting[stream]

    def bytes_until_completion(self, stream, nb_active_streams):
        """Returns the estimated number of bytes to be sent (taking into account
        all active streams!) before the given stream can complete.
        """
        if stream.remaining_size <= self.QUANTUM:
            # Stream completes in the current burst
            return stream.remaining_size
        # Number of sending opportunities necessary to send all data
        nb_slots = math.ceil(stream.remaining_size / self.QUANTUM)
        g = (nb_active_streams - 1) / nb_active_streams
        return self.QUANTUM * (g * (nb_slots - 1) + nb_slots)

    def compute_next_active_stream(self, active_streams):
        """Sets [self.current_stream] to the next active stream."""
        nb_streams = len(active_streams)
        try:
            # Find our currently active stream
            self.current_stream_pos = active_streams.index(self.current_stream)
        except ValueError:
            # The current stream disappeared.  Switch to the one at the
            # same position, but be careful that the position is still
            # valid (reset it to 0 otherwise).
            if self.current_stream_pos >= nb_streams:
                self.current_stream_pos = 0
            self.current_stream = active_streams[self.current_stream_pos]
            return
        # Normal case: just switch to the next stream
        self.current_stream_pos += 1
        self.current_stream_pos %= nb_streams
        self.current_stream = active_streams[self.current_stream_pos]

    def schedule(self, active_streams, available_paths, all_paths, now, *args, **kwargs):
        best_path = min(all_paths, key=lambda path: path.delay)
        best_available_path = min(available_paths, key=lambda path: path.delay)
        if best_path in available_paths:
            self.compute_next_active_stream(active_streams)
            #print("best path for", self.current_stream)
            return self.current_stream, best_path, self.QUANTUM / best_path.speed
        # Should be RTTVAR, but our RTT don't vary
        delta = 0
        # Iterate on streams, in case the next stream decides not to use
        # its slot.  But we need to stop once we have looped over all
        # streams.  l[n:] + l[:n] is useful to "cycle" a list so that
        # element n becomes the first element.
        self.compute_next_active_stream(active_streams)
        for stream in active_streams[self.current_stream_pos:] + active_streams[:self.current_stream_pos]:
            self.current_stream = stream
            self.current_stream_pos = active_streams.index(stream)
            k = self.bytes_until_completion(stream, len(active_streams))
            completion_time_best = best_path.delay + k / best_path.speed
            available_path_pessimistic_rtt = (1 + self.waiting[stream] * self.BETA) * (best_available_path.delay + delta)
            #print("len(active_streams) =", len(active_streams))
            #print("bytes_until_completion =", k)
            #print("delta =", delta)
            #print("completion_time_best =", completion_time_best)
            #print("available_path_pessimistic_rtt =", available_path_pessimistic_rtt)
            if completion_time_best < available_path_pessimistic_rtt:
                # The paper is missing the RTT here, it seems to be a mistake
                #completion_time_available = k / best_available_path.speed
                completion_time_available = best_available_path.delay + k / best_available_path.speed
                #print("completion_time_available =", completion_time_available)
                #print("2 * best_path.delay + delta =", 2 * best_path.delay + delta)
                if completion_time_available >= 2 * best_path.delay + delta:
                    #print("decided to wait:", stream)
                    self.waiting[stream] = 1
                    continue
                else:
                    #print("condition 2 failed for", stream)
                    return stream, best_available_path, self.QUANTUM / best_available_path.speed
            else:
                #print("condition 1 failed for", stream)
                self.waiting[stream] = 0
                return stream, best_available_path, self.QUANTUM / best_available_path.speed
        # Return nothing


policies = {
    "FCFS-minRTT": FcfsMinrttPolicy,
#    "random-minRTT": ,
    "SRPT-minRTT": SmallestMinrttPolicy,
#    "RR-minRTT": ,
    "SRPT-ECF": ExactSrptEcfPolicy,
#    "SRPT2-ECF": SrptEcfPolicy,
    "DRR-ECF": DeficitRoundRobinEcfPolicy,
#    "SRPT-ECF": InflightSrptEcfPolicy,
#    "SRPT-stickyminRTT": StickyMinRTT,
#    "SRPT-stickyminRTT2": StickyMinRTTNonGreedy,
    "SA-ECF": StreamAwareECF,
}
