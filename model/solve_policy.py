#!/usr/bin/env python3

import argparse
import sys

import discrete_event
import graphical
import scheduling
import utils


if __name__ == '__main__':
    policy_name = sys.argv[1]
    instance_file = sys.argv[2]
    headless = (len(sys.argv) > 3)
    Policy = scheduling.policies[policy_name]
    policy = Policy()
    instance = utils.ProblemInstance(instance_file)
    solution = discrete_event.run_simulation(policy, instance)
    solution = utils.normalize_solution(solution)
    if headless:
        completion_times = utils.compute_completion_times(instance, solution)
        for t in completion_times:
            print(t)
    else:
        utils.print_solution(solution)
        utils.print_completion_times(instance, solution)
        graphical.display_solution(instance, solution, policy_name, instance_file)
