"""
Various ways of generating streams and sending data to them.
"""

# Copyright Baptiste Jonglez, Univ. Grenoble Alpes, 2019
#
# <baptiste.jonglez at univ-grenoble-alpes dot fr>
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.

import math

import salabim as sim

from components import Stream


class StreamGenerator(sim.Component):
    """Base class for all stream generators"""

    def weight(self, size):
        """By default, all streams have weight 1"""
        return 1


class WeightBySizeMixin(object):
    """Optional mixin to set the weight inversely proportionnal to the stream size"""

    def weight(self, size):
        # Any stream larger than 100 KB gets a weight of 1
        return math.ceil(100*1000 / size)


class SimpleStreamGenerator(StreamGenerator):
    """Given a list of stream sizes, start streams simultaneously"""

    def setup(self, connection, stream_sizes, wait_time=None):
        """[wait_time] and [stream_size] should be salabim distribution objects."""
        self.connection = connection
        self.stream_sizes = stream_sizes
        self.wait_time = wait_time

    def process(self):
        self.enter(self.connection.stream_generators)
        # Wait a bit
        if self.wait_time:
            yield self.hold(self.wait_time.sample())
        # Create all streams
        for size in self.stream_sizes:
            size_sample = size.sample()
            weight = self.weight(size_sample)
            Stream(connection=self.connection, data=size_sample, weight=weight).close_defer()
        self.leave(self.connection.stream_generators)
        self.connection.schedule()


class StagedStreamGenerator(sim.Component):
    """Given a list of stream sizes and a time interval (in ms) to wait
    between starting streams, start the streams in sequence
    """

    def setup(self, connection, stream_sizes, interval, burst=1, wait_time=None):
        """[wait_time], [stream_size] and [interval] should be salabim distribution objects."""
        self.connection = connection
        self.stream_sizes = stream_sizes
        self.interval = interval
        self.burst = burst
        self.wait_time = wait_time

    def process(self):
        self.enter(self.connection.stream_generators)
        # Wait a bit
        if self.wait_time:
            yield self.hold(self.wait_time.sample())
        # Create an initial burst of streams
        for size in self.stream_sizes[:self.burst]:
            Stream(connection=self.connection, data=size.sample()).close_defer()
        # Create subsequent streams
        for size in self.stream_sizes[self.burst:]:
            yield self.hold(self.interval.sample())
            Stream(connection=self.connection, data=size.sample()).close_defer()
        self.leave(self.connection.stream_generators)
        self.connection.schedule()


class HTTP2StreamGenerator(sim.Component):
    """Simple model of a HTTP2 communication: send a web page (between 30KB
    and 60KB), and when it's fully sent, send several images of random
    size.

    """
    def setup(self, connection, wait_time, image_size, nb_images):
        """[wait_time] and [image_size] should be salabim distribution objects."""
        self.connection = connection
        self.wait_time = wait_time
        self.image_size = image_size
        self.nb_images = nb_images

    def process(self):
        self.enter(self.connection.stream_generators)
        # Wait a bit
        yield self.hold(self.wait_time.sample())
        # Header
        s = Stream(connection=self.connection, data=500)
        # Wait for 20 ms
        yield self.hold(sim.Exponential(20).sample())
        # Body
        s.add_data(sim.Uniform(30000, 60000).sample())
        yield self.wait((s.state, s.STATE_FINISHED))
        s.close()
        # Images
        for i in range(self.nb_images):
            Stream(connection=self.connection, data=self.image_size.sample()).close_defer()
        self.leave(self.connection.stream_generators)
        self.connection.schedule()


class VideoStream(sim.Component):
    """A video stream that sends at a constant bit rate."""
    def setup(self, connection, wait, throughput, duration):
        self.connection = connection
        self.env = connection.env
        self.wait_time = wait
        self.throughput = throughput # in kbit/s
        self.duration = duration # in ms
        self.interval = 5 # in ms
        self.quantum = self.throughput * self.interval / 8 # in bytes

    def process(self):
        self.enter(self.connection.stream_generators)
        # Wait a bit
        yield self.hold(self.wait_time.sample())
        # Create stream
        s = Stream(connection=self.connection, data=self.quantum)
        end_date = self.env.now() + self.duration
        while self.env.now() < end_date:
            yield self.hold(self.interval)
            s.add_data(self.quantum)
        yield self.wait((s.state, s.STATE_FINISHED))
        s.close()
        self.leave(self.connection.stream_generators)
        self.connection.schedule()


