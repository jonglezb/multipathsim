Multi-stream multi-path simulator
=================================

**Multipathsim** is a discrete-event simulator to help explore various scheduling strategies for **MPQUIC**.

Besides multipath, an interesting aspect of MPQUIC is the "multi-stream" dimension, because several streams compete
for network ressources within a single connection.

The simulator does not pretend to be completely realistic (NS-2 or NS-3 would be a better option for this).
For instance, we do not simulate packet loss and retransmission.
However, it allows to precisely understand how a given scheduling strategy behaves in certain situations.

In addition, a goal is to make it as simple as possible to write a new scheduling strategy.  Simple strategies
can be written in 3 lines of Python, while more elaborated strategies require around 20 lines of code.
The most complex scheduler we implemented so far (SA-ECF) takes 60 lines of code.


Topology and paths
==================

Currently there is a single hard-coded topology, consisting of two parallel paths with different RTT and capacity.
In the future, it will be possible to choose from a set of pre-defined topologies or build a custom topology.

Each path is currently modelled as three components:

- a FCFS queue of infinite size, representing the bottleneck buffer
- a "server" with a constant service time of `1/capacity`
- an additional propagation delay applied to packets (currently deterministic and constant over time)

The bottleneck buffer is infinite to avoid dealing with packet loss.  However, an "optimal" congestion
window is computed to limit the amount of packets in the bottleneck buffer.  This optimal CWIN is currently
computed as slightly more than the BDP of the link, which is an ideal case (no bufferbloat at all while keeping
the link busy).


Scenario
========

A **scenario** defines how streams are created, that is, their size (amount of data to send)
and their creation date.

The scenario can be selected with the `-S` option.

Here is a description of a few available scenarios to get an idea:

- `simple_http2`: 10 streams are created simultaneously, each with a random size (exponential distribution with mean 64 KB)
- `simple_http2_weighted`: same scenario, but streams have a "weight" equal to `100 KB/size`.  Currently only the `SA-ECF` scheduler (which uses Weighted Round-Robin) takes weights into account.
- `lots_identical_small_streams`: 200 small streams of size 4 KB are created simultaneously
- `lots_identical_small_streams_staged`: 200 small streams of size 4 KB are created one after another.  The goal is to exercise schedulers in an online setting, where they do not know about all streams in advance.  The creation rate is chosen so that the required network capacity is just above the capacity offered by a single path of the default topology.

Scheduler
=========

Each time a path is available for sending, the scheduler receives the state of the system (streams and paths) and
must take a decision.

The scheduling policy can be selected with the `-P` option.

Schedulers can be classified according to the way they choose the stream to serve when there are several active streams:

- First-Come-First-Serve (FCFS): serve oldest stream first
- Round-Robin (RR) and Weighted Round-Robin (WRR): serve all active streams in sequence
- Shortest Remaining Processing Time (SRPT): serve the stream with the smallest remaining amount of data to send

Then there is the strategy used to select the path, among which:

- MinRTT
- "sticky" minRTT: once a stream starts sending on a path, it will keep using only that path
- Earliest Completion First (ECF)

So, for instance, the `SRPT-minRTT` policy selects the next stream to serve using SRPT, and sends
data on the available path with the lowest RTT.

Visualisation
=============

With the `--visualisation` option, a graphical window will display the progress of packets
as they are scheduled.

![Animation screenshot](doc/animation.png)

The vertical bars represent the streams, and their height is proportional to the amount of data in
the sending buffer (that is, data either in-flight or unsent).  In-flight data is shown in a more
contrasted color: in this example, streams 5, 6 and 8 have already sent all their data, while stream 9
still has some unsent data.  Stream 7 did not start sending data yet.

The bottom-right part represents the paths, with a queue on the left and a propagation part on the right.
Here, we can see that the bottom path only has in-flight packets for stream 6, while the top path has
packets from streams 5, 8 and 9.

Statistics
==========

With `--stats`, the simulator can measure and output CSV data about the performance of the scheduling policy.
Currently we measure the stream completion time (time between stream creation and stream completion) and
the stream total time (time between t=0 and stream completion).

License
=======

This program is available under the terms of the CeCILL-B license (BSD-compatible), see `LICENSE.txt`.
