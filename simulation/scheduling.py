# Copyright Baptiste Jonglez, Univ. Grenoble Alpes, 2019
#
# <baptiste.jonglez at univ-grenoble-alpes dot fr>
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.

import math
import random

from constants import MAX_SEG_SIZE


class SchedulingPolicy(object):
    def __init__(self, env, *args, **kwargs):
        self.env = env

    def schedule(self, active_streams, available_links, all_links):
        """Given a list of active streams and available links, choose a pair
        (stream, link) for the next packet to send.  If no stream wants to
        transmit, this method should return None.
        """
        raise NotImplemented


class BasicPolicy(SchedulingPolicy):
    def schedule(self, active_streams, available_links, *args, **kwargs):
        return active_streams[0], available_links[0]


class MinRTTPolicyFCFS(SchedulingPolicy):
    """Choose streams in-order, and assign them to the path with lowest RTT"""
    def schedule(self, active_streams, available_links, *args, **kwargs):
        return active_streams[0], min(available_links, key=lambda l: l.srtt)


class MinRTTPolicyRandom(SchedulingPolicy):
    """Choose streams randomly, and assign them to the path with lowest RTT"""
    def schedule(self, active_streams, available_links, *args, **kwargs):
        return random.choice(active_streams), min(available_links, key=lambda l: l.srtt)


class MinRTTPolicySmallest(SchedulingPolicy):
    """Choose smallest streams first, and assign them to the path with lowest RTT"""
    def schedule(self, active_streams, available_links, *args, **kwargs):
        stream = min(active_streams, key=lambda s: s.unsent_data_length())
        link = min(available_links, key=lambda l: l.srtt)
        return stream, link


class MinRTTPolicyRoundRobin(SchedulingPolicy):
    """Serve streams in round-robin, and assign them to the path with lowest RTT"""

    def __init__(self, *args, **kwargs):
        # Very naive: just store index of next stream to serve in list of
        # active streams
        self.next_stream = 0
        super().__init__(self, *args, **kwargs)

    def schedule(self, active_streams, available_links, *args, **kwargs):
        link = min(available_links, key=lambda l: l.srtt)
        if self.next_stream >= len(active_streams):
            self.next_stream = 0
        stream = active_streams[self.next_stream]
        self.next_stream += 1
        return stream, link


class SRCTPolicy(SchedulingPolicy):
    """Shortest Remaining Completion Time, inspired from the classical SRPT
    policy.  Order streams so that we first serve streams that have the
    smallest estimated completion time (i.e. smallest streams first).
    Then apply ECF to schedule their data on the links.
    """
    def link_completion_time(self, link, nb_bytes):
        """Estimate the completion time of transmitting the given amount of bytes
        exclusively on the given link.
        """
        # This is a fractional number of packets, on purpose: the last
        # packet is not full-size, and thus has a lower serialization
        # delay.  This is mostly useful for paths with very low capacity,
        # which can make the serialization delay of a single packet quite
        # large (12 ms for 1 Mbps)
        nb_pkt = nb_bytes / MAX_SEG_SIZE
        # If we have n packets to transmit, the completion time is equal
        # to RTT + n * RTT/CWIN in the ACK-pacing phase.
        return link.srtt + nb_pkt * link.srtt / link.cwin

    def schedule(self, active_streams, available_links, all_links):
        # Completion time is an increasing function of the amount of data
        # to send, so we just select the stream with the smallest
        # remaining amount of data to transmit.  If a stream does not want
        # to transmit, we will give an opportunity to the following
        # streams in order of increasing size.
        ordered_streams = sorted(active_streams, key=lambda s: s.unsent_data_length())
        return self._schedule(ordered_streams, available_links, all_links)

    def _schedule(self, ordered_streams, available_links, all_links):
        # How much data (in bytes) already needs to be transmitted by
        # previous streams in the list (i.e. those with higher priority).
        # This is needed because even if the first stream does not want to
        # transmit, the completion time of the second stream needs to
        # account for the time to transmit the first stream entirely.
        scheduled_data = 0
        # This only works for two links and needs generalisation for more links.
        link1, link2 = all_links
        for stream in ordered_streams:
            scheduled_data += stream.unsent_data_length()
            if self.link_completion_time(link1, scheduled_data) < link2.srtt:
                if link1 in available_links:
                    return stream, link1
                else: # Skip this stream (wait for link1 to become available)
                    continue
            if self.link_completion_time(link2, scheduled_data) < link1.srtt:
                if link2 in available_links:
                    return stream, link2
                else: # Skip this stream (wait for link2 to become available)
                    continue
            # Both links are fine to use
            return stream, available_links[0]
        # No stream wants to transmit, return None


class SRCTInFlightPolicy(SRCTPolicy):
    """Variant of SRCT in which we also account for the "in-flight" data when
    ordering streams.  That is, we order stream by increasing size where
    "size" means "unsent data + in-flight data".

    This helps to avoid priority inversions when streams
    can send bursts of packets.  The reason is that it updates the
    "remaining processing time" after one RTT instead of immediately
    after sending, thus delaying any change in SRPT ordering.

    However, this also gives an unfair advantage to streams that use a
    shorter-RTT path, or to newly-created streams.
    """

    def schedule(self, active_streams, available_links, all_links):
        ordered_streams = sorted(active_streams, key=lambda s: s.unsent_data_length() + s.inflight_length())
        return self._schedule(ordered_streams, available_links, all_links)


class SRCTExactPolicy(SRCTPolicy):
    """Variant of SRCT in which we compute the exact estimated completion time
    and use it to order streams.  This improves stability because it
    avoids "inversion" between streams.
    """

    def optimal_completion_time_twolinks(self, link1, link2, nb_bytes):
        """Optimal completion time to send [nb_bytes] bytes on two links.
        """
        # Ensure link1 has lower RTT than link2
        if link1.srtt > link2.srtt:
            link1, link2 = link2, link1
        nb_pkt = nb_bytes / MAX_SEG_SIZE
        # Capacity expressed in MSS / time unit
        capacity1 = link1.cwin / link1.srtt
        capacity2 = link2.cwin / link2.srtt
        return link1.srtt + (nb_pkt + capacity2 * (link2.srtt - link1.srtt)) / (capacity1 + capacity2)

    def estimated_completion_time(self, nb_bytes, all_links):
        """Compute the estimated time needed to transfer [nb_bytes] using multiple
        paths."""
        # TODO: need recursive computation for > 2 paths
        link1, link2 = all_links
        if self.link_completion_time(link1, nb_bytes) < link2.srtt:
            return self.link_completion_time(link1, nb_bytes)
        if self.link_completion_time(link2, nb_bytes) < link1.srtt:
            return self.link_completion_time(link2, nb_bytes)
        # Use both paths
        return self.optimal_completion_time_twolinks(link1, link2, nb_bytes)

    def estimated_stream_completion_time(self, stream, all_links):
        remaining_data_completion_time = self.estimated_completion_time(stream.unsent_data_length(), all_links)
        in_flight_latest_arrival_date = max([path.in_flight_latest_arrival_date(stream) for path in all_links])
        return max(remaining_data_completion_time,
                   in_flight_latest_arrival_date - self.env.now())

    def schedule(self, active_streams, available_links, all_links):
        ordered_streams = sorted(active_streams, key=lambda s: self.estimated_stream_completion_time(s, all_links))
        return self._schedule(ordered_streams, available_links, all_links)


class StickyMinRTT(SchedulingPolicy):
    """Scheduler that tries to be optimal for the
    lots_identical_small_streams_staged scenario.

    It behaves a bit like a "sticky" version of MinRTT: it will send all
    packets belonging to a given stream on a single path.  This path will
    be the lowest-RTT path whenever possible, but if it's not possible we
    prefer sending all packets of the stream to the highest-RTT path.
    This will leave space for other streams that want to use the
    lowest-RTT path.

    We select smaller streams first.
    """
    def __init__(self, *args, **kwargs):
        # Keep a mapping from stream to path
        self.stream_to_path = dict()
        super().__init__(self, *args, **kwargs)

    def schedule(self, active_streams, available_links, *args, **kwargs):
        for stream in sorted(active_streams, key=lambda s: s.unsent_data_length()):
            if stream in self.stream_to_path:
                # Known stream
                link = self.stream_to_path[stream]
                if link in available_links:
                    return stream, link
                else:
                    # Wait for link to become available, schedule another
                    # stream.
                    continue
            else:
                # New stream, select lowest-RTT path
                link = min(available_links, key=lambda l: l.srtt)
                self.stream_to_path[stream] = link
                return stream, link
        return None


class StickyMinRTTNonGreedy(SchedulingPolicy):
    """Variant of StickyMinRTT.

    StickyMinRTT decides to use a path whenever there is space for
    a single packet on this path.  Subsequent packets from the same stream
    will have to wait for more space to become available.

    Instead, we estimate whether *all* packets from the given stream will
    immediately fit on a path before committing to using that path.  If
    it's not possible, we fall back to the lowest-RTT path.

    """
    def __init__(self, *args, **kwargs):
        # Keep a mapping from stream to path
        self.stream_to_path = dict()
        super().__init__(self, *args, **kwargs)

    def schedule(self, active_streams, available_links, *args, **kwargs):
        for stream in sorted(active_streams, key=lambda s: s.unsent_data_length()):
            if stream in self.stream_to_path:
                # Known stream
                link = self.stream_to_path[stream]
                if link in available_links:
                    return stream, link
                else:
                    # Wait for link to become available, schedule another stream.
                    continue
            else:
                # New stream, estimate if a path has enough space for us
                pkt_needed = math.ceil(stream.unsent_data_length() / MAX_SEG_SIZE)
                for link in sorted(available_links, key=lambda l: l.srtt):
                    available = link.cwin - len(link.inflight)
                    if available >= pkt_needed:
                        self.stream_to_path[stream] = link
                        return stream, link
                # No path fits, take the first one
                link = min(available_links, key=lambda l: l.srtt)
                self.stream_to_path[stream] = link
                return stream, link
        return None


class StreamAwareECF(SchedulingPolicy):
    """SA-ECF scheduling strategy from: Rabitsch, P. Hurtig, and
    A. Brunstrom. A Stream-Aware Multipath QUIC Scheduler for
    Heterogeneous Paths. In Proceedings of the Workshop on the Evolution,
    Performance, and Interoperability of QUIC, EPIQ’18, pages 29–35, New
    York, NY, USA, 2018. ACM.

    It's basically Weighted Round Robin + ECF with some subtleties.

    """
    # Hysteresis constant.  The paper gives no value...
    BETA = 1

    def __init__(self, *args, **kwargs):
        # Stream currently being served.
        self.active_stream = None
        # Position of this stream in the list of active streams.
        self.active_stream_position = 0
        # How many more packets will be sent from this stream before we
        # move on to the next stream.  Once a stream starts being served,
        # we don't interrupt it until its remaining quantum reaches 0 (or
        # there's no more data to send), even if new streams become active
        # in the meantime.
        self.remaining_pkt_quantum = 0
        super().__init__(self, *args, **kwargs)

    def packets_until_completion(self):
        """Returns the estimated number of packets to be sent (taking into account
        all streams!) before the currently active stream can complete."""
        pkt_left = math.ceil(self.active_stream.unsent_data_length() / MAX_SEG_SIZE)
        if pkt_left <= self.remaining_pkt_quantum:
            # Stream completes in the current burst
            return float(pkt_left)
        normalized_weight = self.active_stream.relative_weight()
        g = (1 - normalized_weight) / normalized_weight
        return g * (pkt_left - 1) + pkt_left

    def compute_next_active_stream(self, active_streams):
        """Sets [self.active_stream] to the next active stream."""
        if self.active_stream == None or not self.active_stream.is_ready():
            # The active stream no longer has anything to send, so it has
            # been removed from the list of active streams.  Just keep the
            # same position in the list to pick the next stream to send
            # (unless it was the last one, in which case we restart from
            # the head of the list).
            assert(self.active_stream not in active_streams)
            self.active_stream_position %= len(active_streams)
            self.remaining_pkt_quantum = 0
        elif self.remaining_pkt_quantum == 0:
            # Stream still has some data to send, but its burst has been
            # fully consumed.  Switch to next stream.
            self.active_stream_position += 1
            self.active_stream_position %= len(active_streams)
        # New stream (covers both cases of the if/elif above)
        if self.remaining_pkt_quantum == 0:
            self.active_stream = active_streams[self.active_stream_position]
            # Compute new quantum
            self.remaining_pkt_quantum = math.floor(self.active_stream.weight_factor())
            assert(self.remaining_pkt_quantum > 0)
        # Otherwise: do nothing (same stream remains active)

    def schedule(self, active_streams, available_links, all_links, *args, **kwargs):
        best_path = min(all_links, key=lambda l: l.srtt)
        best_available_path = min(available_links, key=lambda l: l.srtt)
        #print("Active stream at start:", self.active_stream)
        if best_path in available_links:
            self.compute_next_active_stream(active_streams)
            self.remaining_pkt_quantum -= 1
            return self.active_stream, best_path
        else:
            delta = max(best_path.rttvar, best_available_path.rttvar)
            self.compute_next_active_stream(active_streams)
            # Used to detect when we have looped over all streams and no
            # one wants to transmit.
            first_stream = self.active_stream
            #print()
            while True:
                if not hasattr(self.active_stream, "waiting"):
                    self.active_stream.waiting = 0
                k = self.packets_until_completion()
                # In our case, cwin and k are both expressed in packets,
                # so the expression below is homogeneous.
                completion_time_best = best_path.srtt + best_path.srtt * k / best_path.cwin
                available_path_pessimistic_rtt = (1 + self.active_stream.waiting * self.BETA) * (best_available_path.srtt + delta)
                #print(self.active_stream)
                #print("packets_until_completion =", k)
                #print("delta =", delta)
                #print("completion_time_best =", completion_time_best)
                #print("available_path_pessimistic_rtt =", available_path_pessimistic_rtt)
                if completion_time_best < available_path_pessimistic_rtt:
                    # There seems to be a mistake in the paper: they
                    # simply compute RTT * k / CWIN.
                    completion_time_available = best_available_path.srtt + best_available_path.srtt * k / best_available_path.cwin
                    #print("completion_time_available =", completion_time_available)
                    #print("2 * best_path.srtt + delta =", 2 * best_path.srtt + delta)
                    if completion_time_available >= 2 * best_path.srtt + delta:
                        #print(self.active_stream, "decides to wait")
                        self.active_stream.waiting = 1
                        # Reset stream burst, so that we serve the next stream.
                        self.remaining_pkt_quantum = 0
                        self.compute_next_active_stream(active_streams)
                        if self.active_stream == first_stream:
                            # We have visited all streams and no one wants to transmit.
                            return
                        continue
                    else:
                        self.remaining_pkt_quantum -= 1
                        return self.active_stream, best_available_path
                else:
                    self.active_stream.waiting = 0
                    self.remaining_pkt_quantum -= 1
                    return self.active_stream, best_available_path


# (stream selection policy)-(path scheduling strategy)
policies = {
    "FCFS-minRTT": MinRTTPolicyFCFS,
    "random-minRTT": MinRTTPolicyRandom,
    "SRPT-minRTT": MinRTTPolicySmallest,
    "RR-minRTT": MinRTTPolicyRoundRobin,
    "SRPT2-ECF": SRCTPolicy,
    "SRPT-ECF": SRCTInFlightPolicy,
    "SRPTexact-ECF": SRCTExactPolicy,
    "SRPT-stickyminRTT": StickyMinRTT,
    "SRPT-stickyminRTT2": StickyMinRTTNonGreedy,
    "SA-ECF": StreamAwareECF,
}
