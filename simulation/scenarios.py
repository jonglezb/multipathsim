"""
Definition of several experiment scenarios, that is, definition of which
streams arrive and when.
"""

# Copyright Baptiste Jonglez, Univ. Grenoble Alpes, 2019
#
# <baptiste.jonglez at univ-grenoble-alpes dot fr>
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.

import salabim as sim

import streamgenerators as g


# Automatically filled by "register" decorator
scenarios = dict()

def register(f):
    scenarios[f.__name__] = f

@register
def two_streams_inversion(connection):
    """Situation with two streams where SRPT2-ECF experiences priority
    inversions.
    """
    streams = [sim.Constant(25000), sim.Constant(26000)]
    g.SimpleStreamGenerator(connection=connection, stream_sizes=streams)


@register
def two_http2_pages(connection):
    g.HTTP2StreamGenerator(connection=connection, wait_time=sim.Exponential(5), image_size=sim.Exponential(64000), nb_images=4)
    g.HTTP2StreamGenerator(connection=connection, wait_time=sim.Exponential(40), image_size=sim.Exponential(64000), nb_images=4)


@register
def simple_http2(connection):
    """Simple HTTP/2 transfer with several streams of random sizes.  Since we
    follow an exponential distribution, we expect to have several smallish
    streams and maybe one or two bigger streams.
    """
    streams = 10*[sim.Exponential(64000)]
    g.SimpleStreamGenerator(connection=connection, stream_sizes=streams)


@register
def simple_http2_weighted(connection):
    """Simple HTTP/2 transfer with several streams of random sizes.  Since we
    follow an exponential distribution, we expect to have several smallish
    streams and maybe one or two bigger streams.
    """
    class WeightedStreamGenerator(g.WeightBySizeMixin, g.SimpleStreamGenerator):
        pass
    streams = 10*[sim.Exponential(64000)]
    WeightedStreamGenerator(connection=connection, stream_sizes=streams)


@register
def video_stream(connection):
    g.VideoStream(connection=connection, wait=sim.Exponential(10), throughput=5000, duration=2000)


@register
def lots_identical_small_streams(connection):
    """Start lots of small streams, all at the same time and of the same size"""
    streams = 200*[sim.Constant(4000)]
    g.SimpleStreamGenerator(connection=connection, stream_sizes=streams)


@register
def lots_identical_small_streams_staged(connection):
    """Start lots of small streams of the same size, staged to begin one after another"""
    streams = 200*[sim.Constant(4000)]
    # With 10 ms between streams, the throughput is just above the
    # capacity of the low-RTT link, so some packets have to go through the
    # high-RTT link.
    g.StagedStreamGenerator(connection=connection,
                            stream_sizes=streams,
                            interval=sim.Constant(10),
                            burst=3)


@register
def lots_small_streams_staged(connection):
    """Start lots of small streams of similar sizes, staged to begin one after another"""
    streams = 200*[sim.Uniform(3800, 4200)]
    # Ideal case for ECF: smaller streams arrive later
    #streams = [sim.Constant(4100-i) for i in range(200)]
    g.StagedStreamGenerator(connection=connection,
                            stream_sizes=streams,
                            interval=sim.Constant(10),
                            burst=3)
