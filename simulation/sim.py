#!/usr/bin/env python3

# Copyright Baptiste Jonglez, Univ. Grenoble Alpes, 2019
#
# <baptiste.jonglez at univ-grenoble-alpes dot fr>
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.

from __future__ import division

import argparse
from datetime import datetime
import random
import sys

import salabim as sim

from constants import *
import components
import networks
import scheduling
import scenarios


def do_animation(env, subflows):
    env.speed(10)
    env.animate(True)
    for i, subflow in enumerate(subflows):
        link = subflow.link
        xbase = 700
        ybase = 10 + 70*(len(subflows) - i)
        # Queue
        pkt_width = QUEUE_PKT_WIDTH + QUEUE_PKT_SPACE
        sim.AnimateQueue(link.queue, x=xbase, y=ybase, title="", direction='w', id='queue')
        sim.AnimateRectangle((xbase+2, ybase-2, xbase - QUEUE_WIDTH, ybase+QUEUE_HEIGHT+2),
                             fillcolor="azure", linecolor="black")
        sim.AnimateRectangle((xbase+1, ybase-1, xbase - QUEUE_WIDTH, ybase+QUEUE_HEIGHT+1),
                             fillcolor="azure", linecolor="azure")
        # Propagation, including the maximum serialisation delay
        sim.AnimateQueue(link.propagating, x=xbase+5, y=ybase, title="", direction='w')
        sim.AnimateRectangle((xbase + 5, ybase, xbase + 5 + PROPAG_PX_PER_MS * (link.rtt.mean() + link.serialisation_delay()), ybase + QUEUE_HEIGHT),
                             fillcolor="90%gray", linecolor="black")
        # Statistics
        sim.AnimateText(text=lambda s, t: "CWIN={}".format(s.cwin),
                        textcolor=lambda s, t: "red" if s.cwin_limited() else "black",
                        x=xbase-150, y=ybase-25, arg=subflow)
        sim.AnimateText(text="C={:.2f} Mbps".format(subflow.link.capacity/1000), x=xbase-80, y=ybase-25)
        sim.AnimateText(text=lambda s, t: "SRTT={:.2f}ms".format(s.srtt), x=xbase+20, y=ybase-25, arg=subflow)


if __name__ == '__main__':
    exp_id = datetime.strftime(datetime.now(), "%Y%m%d-%H%M%S-{}".format(random.randint(0, 100000)))
    stats = ['stream_completion_time', 'stream_total_time', 'stream_stretch']
    parser = argparse.ArgumentParser(description='Multipath multistream scheduling simulation.')
    parser.add_argument('--trace', action='store_true',
                        help='Trace all events')
    parser.add_argument('--visualisation', action='store_true',
                        help='Start a visualisation of the simulation')
    parser.add_argument('--slow-start', action='store_true',
                        help='Use slow-start to increase the congestion window at first')
    parser.add_argument('--no-slow-start', dest='slow_start', action='store_false',
                        help='Start directly at optimal CWIN (default behaviour)')
    parser.add_argument('--pacing', choices=['yes', 'no', 'perfect'], default='no',
                        help='Use packet pacing to avoid bursts.  Perfect pacing uses path characteristics to ensure that the bottleneck buffer is always empty.  Default: no')
    parser.add_argument('--seed', type=int, required=False,
                        help='Random seed of the simulation')
    parser.add_argument('--stats', nargs='*', choices=stats,
                        help='Output statistics from a monitor')
    parser.add_argument('--network', '-N',
                        choices=networks.networks.keys(),
                        default="default_network",
                        help='Network topology to use')
    parser.add_argument('--scenario', '-S', required=True,
                        choices=scenarios.scenarios.keys(),
                        help='Experiment scenario to use')
    parser.add_argument('--policy', '-P', required=True,
                        choices=scheduling.policies.keys(),
                        help='Scheduling policy to use')
    args = parser.parse_args()
    # Parse policy
    Policy = scheduling.policies[args.policy]
    # Start environment
    if args.seed:
        env = sim.Environment(trace=args.trace, random_seed=args.seed)
    else:
        env = sim.Environment(trace=args.trace)
    # Create network
    network = networks.networks[args.network]
    links = network(args.pacing)
    # Create connection with its scheduler
    connection = components.Connection(environment=env, policy=Policy(env), links=links, slow_start=args.slow_start, pacing=args.pacing)
    # Setup stream generators
    scenario = scenarios.scenarios[args.scenario]
    scenario(connection)
    if args.visualisation:
        do_animation(env, connection.subflows)
    # Run simulation
    env.run()
    # Print results
    seed = str(args.seed) if args.seed != None else "1234567"
    slow_start = "slow-start" if args.slow_start else "no-slow-start"
    if args.stats:
        for stat in args.stats:
            monitor = getattr(connection, stat)
            for value in monitor.x():
                print(",".join([exp_id, seed, slow_start, args.pacing, args.scenario, args.policy, stat, str(value)]))
