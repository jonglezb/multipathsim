# Copyright Baptiste Jonglez, Univ. Grenoble Alpes, 2019-2020
#
# <baptiste.jonglez at univ-grenoble-alpes dot fr>
#
# This software is governed by the CeCILL-B license under French law and
# abiding by the rules of distribution of free software.  You can  use, 
# modify and/ or redistribute the software under the terms of the CeCILL-B
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info". 
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability. 
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or 
# data to be ensured and,  more generally, to use and operate it in the 
# same conditions as regards security. 
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL-B license and that you accept its terms.

from __future__ import division

from collections import defaultdict
import math
import random

import intervals as I
import salabim as sim

from constants import *
import utils


class Stream(sim.Component):
    STATE_OPEN = 'open'         # There is still unacked data
    STATE_FINISHED = 'finished' # All data has been sent and acked
    STATE_CLOSED = 'closed'     # Stream has been explicitly closed

    def setup(self, connection, data, weight=1):
        self.connection = connection
        self.env = connection.env
        # Absolute weight of this stream, as integer.  We don't implement
        # a HTTP2-style "dependency tree" between streams, so this weight
        # is simply compared with the weight of all other "ready" streams
        # (i.e. streams that have data to send).
        # Note that a scheduler may ignore weights altogether!
        assert(weight > 0)
        self.weight = weight
        # All these intervals represent ranges of sequence numbers.  By
        # convention all ranges are expressed as [x, y) which covers all
        # bytes from x to y-1, for a length of y-x.  This allows to easily
        # take the union, difference and intersection of adjacent
        # intervals while keeping the "open on the left" property.
        self.data = I.closedopen(0, int(data))
        self.acked = I.empty()
        self.inflight = I.empty()
        self.state = sim.State('stream_state', value=(self.STATE_OPEN if data > 0 else self.STATE_FINISHED))
        # Whether the stream should be closed as soon as it is finished.
        self.want_close = False
        # Used to compute completion time
        self.start_time = self.env.now()
        # Used for the visualisation of packets
        self.color = random.choice([color for color in sorted(sim.colornames().keys()) if self.env.is_dark(color)])
        self.lighter_color = (self.color, 128)
        self.position = self.connection.stream_position(self)
        # Add ourselves to the list of streams
        self.enter(self.connection.streams)
        # Start animation
        self.animations = list()
        self.animate()

    def close(self):
        """Close stream.  This is only possible for a finished stream."""
        assert(self.state.get() == self.STATE_FINISHED)
        self.state.set(self.STATE_CLOSED)
        self.activate()

    def close_defer(self):
        """Instruct the stream to close itself as soon as it is finished."""
        self.want_close = True

    def unsent_data(self):
        """Returns an Interval representing data that has never been sent on any
        path.  This interval *might* contain holes if a scheduling policy
        decides to send data out-of-order.
        """
        return self.data - self.acked - self.inflight

    def unsent_data_length(self):
        return utils.length(self.unsent_data())

    def inflight_length(self):
        return utils.length(self.inflight)

    def buffered_data(self):
        """Returns an Interval representing data that is still in the buffer.
        Every ACKed data at the head falls out of this interval."""
        # No ACKed data, or the ACKed data is not at head
        if utils.length(self.acked) == 0 or self.acked.lower > 0:
            return self.data
        # Normal case: remove first block of ACKed data
        return self.data - self.acked[0]

    def is_ready(self):
        """Is this stream ready to send more data?"""
        return self.unsent_data_length() > 0

    def is_all_acked(self):
        """Has all data on this stream been ACKed?"""
        return self.data == self.acked

    def relative_weight(self):
        """Returns the relative weight of this stream compared to the summed
        weight of all other "ready" streams, as a floating point number
        between 0 and 1.
        """
        streams = self.connection.ready_streams()
        total_weight = sum(s.weight for s in streams)
        if total_weight == 0:
            return 1.
        return self.weight / total_weight

    def weight_factor(self):
        """Returns the weight of this stream divided by the smallest weight
        amongst active streams, as a floating point number.
        """
        streams = self.connection.ready_streams()
        min_weight = min(s.weight for s in streams)
        assert(min_weight > 0)
        return self.weight / min_weight

    def add_data(self, nb_bytes):
        self.data = self.data.apply(lambda x: (x.left, x.lower, x.upper + int(nb_bytes), x.right))
        if self.state.get() == self.STATE_FINISHED:
            # Restart stream
            self.start_time = self.env.now()
            self.state.set(self.STATE_OPEN)
        self.activate()

    def next_unsent(self):

        """Returns the sequence number of the first unsent byte."""
        unsent = self.unsent_data()
        if unsent.is_empty():
            return
        return unsent.lower

    def create_packet(self, max_packet_length, subflow):
        """Returns the next packet to be sent"""
        next_unsent = self.next_unsent()
        seqnum_interval = I.closedopen(next_unsent, next_unsent + max_packet_length)
        # Ensure we don't send more data than the total
        seqnum_interval &= self.data
        assert(seqnum_interval.is_atomic())
        # Record this data as being in-flight
        self.inflight |= seqnum_interval
        # Create packet
        return Packet(connection=self.connection, seqnum=seqnum_interval.lower,
                      payload_length=utils.length(seqnum_interval),
                      subflow=subflow,
                      stream=self)

    def packet_acked(self, packet):
        interval = I.closedopen(packet.seqnum, packet.seqnum + packet.payload_length)
        self.acked |= interval
        # Somewhat subtle here: there may be several copies of the same
        # data in flight, but we consider that once the data has been
        # ACKed once, we don't care about the other copies anymore.  Of
        # course, we can do that only because we don't manage the
        # congestion window here.
        self.inflight &= ~interval
        # Stream has possibly finished
        self.activate()

    def process(self):
        while True:
            if self.state.get() == self.STATE_CLOSED:
                break
            if self.is_ready():
                self.connection.schedule()
            if self.is_all_acked() and self.state.get() == self.STATE_OPEN:
                # Stream has finished sending its current data, but may receive further data
                self.state.set(self.STATE_FINISHED)
                if self.want_close:
                    self.state.set(self.STATE_CLOSED)
                    break
            yield self.passivate()
        # Close stream: record statistics
        completion_time = self.env.now() - self.start_time
        self.connection.stream_completion_time.tally(completion_time)
        self.connection.stream_total_time.tally(self.env.now())
        optimal_completion_time = self.connection.optimal_completion_time(self.data.upper)
        self.connection.stream_stretch.tally(completion_time / optimal_completion_time)
        # Remove animations
        for obj in self.animations:
            obj.remove()
            del obj
        # Remove ourselves from the list of streams
        self.leave(self.connection.streams)
        self.connection.schedule()

    def animate(self):
        xbase = 20 + 40*self.position
        ybase = 300
        # Display stream ID
        stream_id = sim.AnimateText(text=str(self.sequence_number()), x=xbase, y=ybase-5, text_anchor="n")
        self.animations.append(stream_id)
        # Display stream as a rectangle
        rectangle_buffered = lambda stream, t: stream.rectangle_buffered(t, xbase, ybase)
        self.animations.append(sim.AnimateRectangle(spec=rectangle_buffered, arg=self, linecolor="black", fillcolor=self.lighter_color))
        # Loop on in-flight blocks.  We create a number of rectangles
        # which might not be shown depending on the number of blocks.
        for i in range(20):
            rectangle_inflight = lambda stream_i, t: stream_i[0].rectangle_inflight(t, stream_i[1], xbase, ybase)
            self.animations.append(sim.AnimateRectangle(spec=rectangle_inflight, arg=(self, i), linecolor="black", fillcolor=self.lighter_color))
        # Loop on ACKed blocks (background color)
        for i in range(20):
            rectangle_acked = lambda stream_i, t: stream_i[0].rectangle_acked(t, stream_i[1], xbase, ybase)
            self.animations.append(sim.AnimateRectangle(spec=rectangle_acked, arg=(self, i), linecolor="black", fillcolor="bg"))

    # Visualisation: show data still in buffer (either in flight, ACKed, or unsent)
    def rectangle_buffered(self, t, xbase, ybase):
        width = 20
        bytes_per_pixel = 350
        x0 = xbase - width/2
        x1 = xbase + width/2
        y0 = ybase
        displayed = utils.length(self.buffered_data())
        y1 = y0 + displayed / bytes_per_pixel
        return (x0, y0, x1, y1)

    def rectangle_generic(self, t, index, xbase, ybase, interval):
        """Factored code for in-flight blocks and acked blocks"""
        if utils.length(interval) == 0 or index >= len(interval):
            # Nothing to show
            return (0, 0, 0, 0)
        #print(self.name(), index, interval[index])
        block = interval[index]
        offset_bytes = block.lower - self.buffered_data().lower
        width = 20
        bytes_per_pixel = 350
        x0 = xbase - width/2
        x1 = xbase + width/2
        y0 = ybase + offset_bytes / bytes_per_pixel
        y1 = y0 + utils.length(block) / bytes_per_pixel
        return (x0, y0, x1, y1)

    def rectangle_inflight(self, t, index, xbase, ybase):
        """Shows the [index]-th block of in-flight data"""
        return self.rectangle_generic(t, index, xbase, ybase, self.inflight)

    def rectangle_acked(self, t, index, xbase, ybase):
        """Shows the [index]-th block of acked data still in the buffer"""
        return self.rectangle_generic(t, index, xbase, ybase, self.acked & self.buffered_data())


class Packet(sim.Component):
    def setup(self, connection, seqnum, payload_length, subflow, stream):
        self.connection = connection
        self.env = connection.env
        # Sequence number with the same convention as TCP: it represents
        # the sequence number of the first data byte in the segment.
        self.seqnum = seqnum
        self.payload_length = payload_length
        self.subflow = subflow
        self.stream = stream
        # When was the packet sent (for RTT measurement)
        self.sent_time = self.env.now()
        # Propagation time for this packet
        self.propagation_time_sample = None

    def animation_objects(self, id):
        if id == 'queue':
            # Used for displaying packets in the queue of a link
            size = max(3, round(QUEUE_PKT_WIDTH * self.payload_length / MAX_SEG_SIZE))
            r = sim.AnimateRectangle((0, 0, -size, QUEUE_HEIGHT),
                                     fillcolor=self.stream.color,
                                     text=str(self.stream.sequence_number()),
                                     fontsize=10)
            return (size + QUEUE_PKT_SPACE), 0, r
        else:
            # Used for displaying packets that are propagating on the link
            size = max(1, math.ceil(PROPAG_PX_PER_MS * self.subflow.link.serialisation_delay(self.payload_length + PKT_OVERHEAD)))
            if size >= 3:
                return 0, 0, sim.Animate(rectangle0=(0, 0, size, QUEUE_HEIGHT),
                                         x1=id.x + PROPAG_PX_PER_MS*self.propagation_time_sample,
                                         y1=id.y,
                                         t0=self.env.now(),
                                         t1=self.env.now() + self.propagation_time_sample,
                                         fillcolor0=self.stream.color,
                                         linecolor0="black",
                                         linewidth0=1)
            else:
                # No space for border
                return 0, 0, sim.Animate(rectangle0=(0, 0, size, QUEUE_HEIGHT-1),
                                         x1=id.x + PROPAG_PX_PER_MS*self.propagation_time_sample,
                                         y1=id.y,
                                         t0=self.env.now(),
                                         t1=self.env.now() + self.propagation_time_sample,
                                         fillcolor0=self.stream.color)

    def process(self):
        # Wait for the Link to handle us
        yield self.passivate()
        # Propagation delay
        self.propagation_time_sample = self.subflow.link.rtt.sample()
        self.enter(self.subflow.link.propagating)
        yield self.hold(self.propagation_time_sample)
        self.subflow.packet_acked(self)
        self.stream.packet_acked(self)
        self.connection.schedule()


class Link(sim.Component):
    """A link is modelled as a queue of finite length containing packets, with
    packets being served at a constant throughput, and an additional RTT
    component modelling propagation delay.
    """

    def setup(self, capacity, rtt, pacing):
        self.perfect_pacing = (pacing == 'perfect')
        self.capacity = capacity # in kbit/s
        self.goodput = capacity * MAX_SEG_SIZE / (MAX_SEG_SIZE + PKT_OVERHEAD)
        self.rtt = rtt # propagation delay in ms (random variable)
        # Packets waiting to be transmitted on the link (router queue)
        self.queue = sim.Queue(self.name() + '.queue')
        # Packets that are propagating on the link.
        self.propagating = sim.Queue(self.name() + '.propagating')

    def serialisation_delay(self, pktsize=MAX_SEG_SIZE+PKT_OVERHEAD):
        """Serialisation delay of a packet on this link (by default, full-size packet)"""
        return 8 * pktsize / self.capacity

    def add_packet(self, pkt):
        pkt.enter(self.queue)
        # activate() seems to cancel a hold(), so we need to check whether
        # the link was actually idle.
        if len(self.queue) == 1:
            self.activate()

    def packet_acked(self, pkt):
        pkt.leave(self.propagating)

    def process(self):
         while True:
            while len(self.queue) == 0:
                yield self.passivate()
            pkt = self.queue[0]
            yield self.hold((pkt.payload_length + PKT_OVERHEAD) * 8 / self.capacity)
            enter_time = pkt.enter_time(self.queue)
            self.queue.pop()
            # Activate the packet ("send it") so that it will experience
            # the link RTT after being served.
            pkt.activate()
            # With perfect pacing, immediately schedule next packet
            if self.perfect_pacing:
                pkt.connection.schedule()


class Connection(sim.Component):
    """A connection hosts several streams that share the same resources.  It
    creates a Subflow for each available Link.

    It then schedules data from the streams to the subflows.

    Additional parameters such as "slow_start" are passed to the Subflow class.
    """
    def setup(self, environment, policy, links, **kwargs):
        self.env = environment
        # Elements of the connection
        self.streams = sim.Queue(self.name() + ".streams")
        self.stream_generators = sim.Queue(self.name() + ".stream_generators")
        self.subflows = [Subflow(connection=self, link=l, **kwargs) for l in links]
        # Scheduling
        self.policy = policy
        # Record completion time of streams from the moment they are
        # created until the moment they are closed.
        self.stream_completion_time = sim.Monitor("stream_completion_time")
        # Record completion time of streams, but relatively to the start
        # of the simulation.  This is a more interesting way to compare
        # scheduling policies when streams have dependencies.
        self.stream_total_time = sim.Monitor("stream_total_time")
        # Record the "stretch" of streams, which is the actual completion
        # time divided by the "optimal completion time" if the stream was
        # alone.  stretch is always >= 1, and the higher it is, the longer
        # a stream had to wait compared to its size.
        self.stream_stretch = sim.Monitor("stream_stretch")

    def stream_position(self, stream):
        """Returns the "position" for a new stream for visualisation, which is
        just an integer index starting from 0.
        """
        # This looks complicated, but we just look for the first hole in
        # the list of existing positions.
        positions = sorted([s.position for s in self.streams])
        if len(positions) == 0 or positions[0] > 0:
            return 0
        for s1, s2 in zip(positions, positions[1:]):
            if s2 - s1 > 1:
                return s1 + 1
        return positions[-1] + 1

    def available_subflows(self):
        return [subflow for subflow in self.subflows if subflow.is_available()]

    def ready_streams(self):
        return [s for s in self.streams if s.is_ready()]

    def is_finished(self):
        # Simulation is finished when there no process generating new data
        # or stream, and all streams have finished transmitting.
        return len(self.stream_generators) == 0 and all([stream.state.get() == stream.STATE_FINISHED for stream in self.streams])

    def try_finish(self):
        if self.is_finished():
            # End simulation
            self.env.main().activate()

    def schedule(self):
        """Run the scheduler"""
        self.activate()

    def optimal_completion_time(self, size):
        """Compute the theoretical optimal completion time for a stream of the
        given size in bytes.
        """
        def path_ct(path, _size):
            """Completion time for a single path"""
            return path.rtt.mean() + 8 * _size / path.goodput
        # With 3 paths or more, it looks a bit complicated to compute
        # (recursion or dynamic programming?).
        if len(self.subflows) == 0 or len(self.subflows) > 2:
            return math.nan
        if len(self.subflows) == 1:
            return path_ct(self.subflows[0].link, size)
        # Case of 2 paths
        l1 = self.subflows[0].link
        l2 = self.subflows[1].link
        if path_ct(l1, size) <= path_ct(l2, 0):
            return path_ct(l1, size)
        if path_ct(l2, size) <= path_ct(l1, 0):
            return path_ct(l2, size)
        return (8*size + l1.rtt.mean() * l1.goodput + l2.rtt.mean() * l2.goodput) / (l1.goodput + l2.goodput)

    def process(self):
        # Main scheduler
        yield self.passivate()
        while True:
            while len(self.ready_streams()) == 0 or len(self.available_subflows()) == 0:
                self.try_finish()
                yield self.passivate()
            result = self.policy.schedule(self.ready_streams(), self.available_subflows(), self.subflows)
            if result == None:
                yield self.passivate()
            else:
                stream, link = result
                link.send_data_from(stream)
                stream.activate()


class Subflow(sim.Component):
    """A subflow is part of a connection and is tied to a specific Link.  It
    keeps track of the RTT of the link by measuring the performance of any
    stream using this link.

    Arguments:

    - slow_start: ramp up progressively to the target CWIN

    - pacing: when not limited by CWIN, pace packets at CWIN/RTT to avoid
    sending large bursts

    - perfect_pacing: keep bottleneck buffer empty at all times
    """
    # Used to compute smoothed RTT estimates (see RFC 6298)
    ALPHA = 1/8
    BETA = 1/4

    def setup(self, connection, link, slow_start, pacing):
        assert(pacing in ['yes', 'no', 'perfect'])
        self.connection = connection
        self.link = link
        # Queue of packets that are in flight
        self.inflight = sim.Queue(self.name() + '.inflight')
        # Congestion window, expressed in multiple of MSS
        if slow_start:
            self.cwin = 3
        else:
            self.cwin = self.optimal_cwin()
        self.perfect_pacing = (pacing == 'perfect')
        # Pacing support
        self.pacing = (pacing == 'yes')
        self.available_pacing = True
        self.last_packet_size = MAX_SEG_SIZE + PKT_OVERHEAD
        # RTT estimation on the link
        self.estimated = False
        self.srtt = link.rtt.sample()
        if self.perfect_pacing:
            # Immediately provide accurate SRTT in "perfect pacing" mode
            self.srtt += link.serialisation_delay()
        self.rttvar = 0
        # For each stream, estimate the date at which the latest packet will be ACKed.
        self.latest_arrival_date = defaultdict(int)
        # Keep statistics
        self.cwin_mon = sim.Monitor(self.name() + '.cwin', level=True, initial_tally=self.cwin)
        self.rtt_mon = sim.Monitor(self.name() + '.rtt')
        self.srtt_mon = sim.Monitor(self.name() + '.srtt', level=True, initial_tally=self.srtt)
        self.rttvar_mon = sim.Monitor(self.name() + '.rttvar', level=True, initial_tally=self.rttvar)

    def packet_acked(self, packet):
        self.rtt_measurement(packet)
        self.update_cwin()
        packet.leave(self.inflight)
        self.link.packet_acked(packet)

    def rtt_measurement(self, pkt):
        t0 = pkt.sent_time
        t1 = self.connection.env.now()
        rtt = t1 - t0
        self.rtt_mon.tally(rtt)
        # In perfect pacing mode, just keep the ideal RTT value.
        if self.perfect_pacing:
            return
        # SRTT is computed the same in RFC6298 and in the Linux kernel,
        # but the RTTVAR computation in Linux is more complicated than the
        # RFC.  Stick to the RFC for simplicity.
        if not self.estimated:
            # First measurement
            self.srtt = rtt
            self.rttvar = rtt/2
            self.estimated = True
        else:
            self.rttvar = (1 - self.BETA) * self.rttvar + self.BETA * abs(self.srtt - rtt)
            self.srtt = (1 - self.ALPHA) * self.srtt + self.ALPHA * rtt
        self.srtt_mon.tally(self.srtt)
        self.rttvar_mon.tally(self.rttvar)

    def optimal_cwin(self):
        bdp = 1 + self.link.rtt.mean() * self.link.capacity / (8 * (MAX_SEG_SIZE + PKT_OVERHEAD))
        return math.ceil(bdp)

    def update_cwin(self):
        """Called when a packet is ACKed.

        We adopt a very simple congestion window modelisation: we stay in
        the slow-start phase (cwin++ for each ACK) until we reach the
        "optimal" point that Cubic would hover around during the congestion
        avoidance phase: cwin = BDP + 75% of the queue.  At this point we
        keep cwin constant.
        """
        if not self.cwin_limited():
            return
        target = self.optimal_cwin()
        if self.cwin >= target:
            self.cwin = target
        else:
            # Slow start: increase cwin for each ACK
            self.cwin += 1
        self.cwin_mon.tally(self.cwin)

    def is_available(self):
        # With perfect pacing, we don't care about CWIN: we are only
        # available if there is no packet in the link queue.
        if self.perfect_pacing:
            return len(self.link.queue) == 0
        # We are available if both:
        # - CWIN is not reached
        # - pacing allows us to send (if pacing is enabled)
        # Logic reminder: "(not A) or B" is equivalent to "A => B"
        return len(self.inflight) < self.cwin and (not self.pacing or self.available_pacing)

    def cwin_limited(self):
        return len(self.inflight) >= self.cwin

    def send_data_from(self, stream):
        packet = stream.create_packet(MAX_SEG_SIZE, self)
        if self.pacing:
            assert(self.available_pacing)
            self.available_pacing = False
            self.last_packet_size = packet.payload_length + PKT_OVERHEAD
            self.activate()
        # Add newly created packet to the "in flight" queue
        packet.enter(self.inflight)
        # Compute estimated arrival time
        self.latest_arrival_date[stream] = max(self.latest_arrival_date[stream],
                                               self.connection.env.now() + self.srtt)
        # Also add packet to the link queue for transmission
        self.link.add_packet(packet)

    def in_flight_latest_arrival_date(self, stream):
        return self.latest_arrival_date[stream]

    def process(self):
        while True:
            # We will be activated each time a packet has just been sent
            # on this subflow (only if pacing is enabled).
            # last_packet_size will be set.
            yield self.passivate()
            # In kbit/s
            estimated_capacity = self.cwin * 8 * (MAX_SEG_SIZE + PKT_OVERHEAD) / self.srtt
            yield self.hold(8 * self.last_packet_size / estimated_capacity)
            self.available_pacing = True
            self.connection.schedule()
